﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Role
    {
        [Required]
        public Guid Guid { get; set; }

        [Required]
        public string Name { get; set; }
    }
}