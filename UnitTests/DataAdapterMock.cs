﻿using DatabaseConnectionLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    internal class DataAdapterMock : IDataAdapter
    {
        private Dictionary<string, List<Dictionary<string, object>>> tableNameDataMap = new Dictionary<string, List<Dictionary<string, object>>>();

        public DataAdapterMock()
        {
            tableNameDataMap.Add("Roles", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("Users", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("DocumentTypes", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("FileCabinets", new List<Dictionary<string, object>>());
            tableNameDataMap.Add("Documents", new List<Dictionary<string, object>>());

        }


        public bool AddData(string tableName, List<Dictionary<string, object>> data)
        {
            if (data.Count > 0)
            {
                foreach (Dictionary<string, object> singleEntry in data)
                {
                    tableNameDataMap[tableName].Add(singleEntry);
                }
                return true;
            }

            return false;
        }

        public bool DeleteData(string tableName, DataQuery query)
        {
            foreach (Dictionary<string, object> row in tableNameDataMap[tableName])
            {

                if (row["id"].ToString() == query.queries["id"].ToString())
                {
                    tableNameDataMap[tableName].Remove(row);
                    return true;
                }
            }

            return false;
        }

        public List<Dictionary<string, object>> GetData(string tableName, DataQuery query)
        {
            List<Dictionary<string, object>> returnedData = new List<Dictionary<string, object>>();

            if (query != null)
            {
                foreach (Dictionary<string, object> row in tableNameDataMap[tableName])
                {
                    if (row["id"].ToString() == query.queries["id"].ToString())
                    {
                        returnedData.Add(row);
                    }
                }

                return returnedData;
            }
            else
            {
                return tableNameDataMap[tableName];
            }
        }

        public void SetSource(string connectionString)
        {
            throw new NotImplementedException();
        }

        public bool UpdateData(string tableName, Dictionary<string, object> newData, DataQuery query)
        {
            int count = tableNameDataMap[tableName].Count;     

            for (int i = 0; i < count; i++)
            {
                if (tableNameDataMap[tableName][i]["id"].ToString() == query.queries["id"].ToString())
                {
                    tableNameDataMap[tableName][i] = newData;
                    return true;
                }
            }

            return false;
        }
    }
}
