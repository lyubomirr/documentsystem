﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseConnectionLayer;

namespace UnitTests
{
    [TestClass]
    public class DataQueryTests
    {
        [TestMethod]
        public void CheckWhereClause()
        {
            DataQuery dataQuery = new DataQuery();
            dataQuery.AddQuery("id", 3);
            dataQuery.AddQuery("deleted", 1);
            dataQuery.AddQuery("file_cabinet_id", 4);
            dataQuery.AddQuery("name", "'document'");

            Assert.AreEqual<string>(" WHERE id=3 AND deleted=1 AND file_cabinet_id=4 AND name='document'", dataQuery.ToWhereClause());
        }
    }
}
