﻿using DataAccessLayer;
using DatabaseConnectionLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Configuration;
using System.Linq;
using Models;

namespace UnitTests
{
    [TestClass]
    public class DataFacadeTests
    {
        private void generateMockData(Role role, User user, DocumentType docType, FileCabinet cab, Document doc)
        {
            role.Guid = Guid.NewGuid();
            role.Name = "Role";

            user.Guid = Guid.NewGuid();
            user.Username = "user";
            user.Password = "pass";
            user.RoleId = role.Guid;

            docType.Guid = Guid.NewGuid();
            docType.Name = "Type";

            cab.Guid = Guid.NewGuid();
            cab.Name = "File Cabinet";

            doc.Guid = Guid.NewGuid();
            doc.Name = "Some doc";
            doc.TypeId = docType.Guid;
            doc.StoreDate = DateTime.Now;
            doc.Archived = false;
            doc.Deleted = false;
            doc.FileCabinetId = cab.Guid;
            doc.FilePath = "some_path";
            doc.Owner = user.Guid;
        }

        private TableMapping GetTableMapping(string tableName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TableMappingConfig));
            TableMappingConfig tableMapping;

            using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "\\TableMappingConfig.xml", FileMode.Open))
            {
                tableMapping = (TableMappingConfig)serializer.Deserialize(fs);
            }

            return tableMapping.TableMappings.FirstOrDefault(i => i.TableName == tableName);
        }
        
        [TestMethod]
        public void DataFacade_ShouldAddData()
        {
            Role role = new Role();
            User user = new User();
            DocumentType docType = new DocumentType();
            FileCabinet cab = new FileCabinet();
            Document doc = new Document();
            generateMockData(role, user, docType, cab, doc);

            var roleMapping = GetTableMapping("Roles");
            var userMapping = GetTableMapping("Users");
            var docTypesMapping = GetTableMapping("DocumentTypes");
            var fileCabsMapping = GetTableMapping("FileCabinets");
            var docMapping = GetTableMapping("Documents");


            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Role>(role, roleMapping.TableName, roleMapping.Pairs);
            dataFacade.AddObject<User>(user, userMapping.TableName, userMapping.Pairs);
            dataFacade.AddObject<DocumentType>(docType, docTypesMapping.TableName, docTypesMapping.Pairs);
            dataFacade.AddObject<FileCabinet>(cab, fileCabsMapping.TableName, fileCabsMapping.Pairs);


            Assert.IsTrue(dataFacade.AddObject<Document>(doc, docMapping.TableName, docMapping.Pairs));
        }

        [TestMethod]
        public void DataFacade_ShouldDeleteData()
        {
            Role role = new Role();
            User user = new User();
            DocumentType docType = new DocumentType();
            FileCabinet cab = new FileCabinet();
            Document doc = new Document();
            generateMockData(role, user, docType, cab, doc);

            var roleMapping = GetTableMapping("Roles");
            var userMapping = GetTableMapping("Users");
            var docTypesMapping = GetTableMapping("DocumentTypes");
            var fileCabsMapping = GetTableMapping("FileCabinets");
            var docMapping = GetTableMapping("Documents");


            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Role>(role, roleMapping.TableName, roleMapping.Pairs);
            dataFacade.AddObject<User>(user, userMapping.TableName, userMapping.Pairs);
            dataFacade.AddObject<DocumentType>(docType, docTypesMapping.TableName, docTypesMapping.Pairs);
            dataFacade.AddObject<FileCabinet>(cab, fileCabsMapping.TableName, fileCabsMapping.Pairs);
            dataFacade.AddObject<Document>(doc, docMapping.TableName, docMapping.Pairs);

            var dq = new DataQuery();
            dq.AddQuery("id","'" + doc.Guid + "'");
            dataFacade.DeleteObject(dq, docMapping.TableName);

            var docs = dataFacade.GetTableData<Document>(null, docMapping.TableName, docMapping.Pairs);

            Assert.AreEqual(0, docs.Count);
        }


        [TestMethod]
        public void DataFacade_ShouldModifyData()
        {
            Role role = new Role();
            User user = new User();
            DocumentType docType = new DocumentType();
            FileCabinet cab = new FileCabinet();
            Document doc = new Document();
            generateMockData(role, user, docType, cab, doc);

            var roleMapping = GetTableMapping("Roles");
            var userMapping = GetTableMapping("Users");
            var docTypesMapping = GetTableMapping("DocumentTypes");
            var fileCabsMapping = GetTableMapping("FileCabinets");
            var docMapping = GetTableMapping("Documents");


            IDataFacade dataFacade = new DatabaseDataFacade(new DataAdapterMock());
            dataFacade.AddObject<Role>(role, roleMapping.TableName, roleMapping.Pairs);
            dataFacade.AddObject<User>(user, userMapping.TableName, userMapping.Pairs);
            dataFacade.AddObject<DocumentType>(docType, docTypesMapping.TableName, docTypesMapping.Pairs);
            dataFacade.AddObject<FileCabinet>(cab, fileCabsMapping.TableName, fileCabsMapping.Pairs);


            dataFacade.AddObject<Document>(doc, docMapping.TableName, docMapping.Pairs);
            doc.Name = "new name";

            var dq = new DataQuery();
            dq.AddQuery("id","'" + doc.Guid + "'");

            dataFacade.ModifyObject<Document>(doc, dq, docMapping.TableName, docMapping.Pairs);

            var docs = dataFacade.GetTableData<Document>(dq, docMapping.TableName, docMapping.Pairs);

            Assert.AreEqual("'new name'", docs[0].Name);

        }


    }
}
