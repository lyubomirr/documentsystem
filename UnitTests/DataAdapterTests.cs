﻿using System;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLayer;
using DatabaseConnectionLayer;
using System.Collections.Generic;
using Interfaces.Data;
using Models;

namespace UnitTests
{

    [TestClass]
    public class DataAdapterTests
    {
        private string testDbConnectionString = "Data Source = LRUZHINSKIAT; Initial Catalog = DocumentSystemTest; Integrated Security = true;";

        private void clearTable(string tableName)
        {
            string cmdText = $"DELETE FROM {tableName};";

            try
            {
                using (SqlConnection conn = new SqlConnection(testDbConnectionString))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(cmdText, conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private void clearDb()
        {
            clearTable("Users");
            clearTable("Roles");
            clearTable("Documents");
            clearTable("DocumentTypes");
            clearTable("FileCabinets");
        }

        private void generateMockData(
            Dictionary<string, object> role, Dictionary<string, object> user, Dictionary<string, object> docType, 
            Dictionary<string, object> cab, Dictionary<string, object> doc 
            )
        {
            role.Add("id","'" + Guid.NewGuid().ToString() + "'");
            role.Add("name", "'Role'");

            user.Add("id", "'" + Guid.NewGuid().ToString() + "'");
            user.Add("username", "'user'");
            user.Add("password", "'pass'");
            user.Add("role_id", role["id"]);

            docType.Add("id", "'" + Guid.NewGuid().ToString() + "'");
            docType.Add("name", "'Type'");

            cab.Add("id", "'" + Guid.NewGuid().ToString() + "'");
            cab.Add("name", "'File Cabinet'");

            doc.Add("id", "'" + Guid.NewGuid().ToString() + "'");
            doc.Add("name", "'Some doc'");
            doc.Add("type_id", docType["id"]);
            doc.Add("store_date", "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            doc.Add("archived", 0);
            doc.Add("deleted", 0);
            doc.Add("file_cabinet_id", cab["id"]);
            doc.Add("file_path", "'path'");
            doc.Add("owner", user["id"]);

        }

        [TestMethod]
        public void DataAdapter_ShouldAddData()
        {
            clearDb();


            Dictionary<string, object> role = new Dictionary<string, object>();
            Dictionary<string, object> user = new Dictionary<string, object>();
            Dictionary<string, object> docType = new Dictionary<string, object>();
            Dictionary<string, object> cab = new Dictionary<string, object>();
            Dictionary<string, object> doc = new Dictionary<string, object>();

            generateMockData(role, user, docType, cab, doc);

            Interfaces.Data.IDataAdapter dataAdapter = new DatabaseDataAdapter(testDbConnectionString);
            dataAdapter.AddData("Roles", new List<Dictionary<string, object>>() { role });
            dataAdapter.AddData("Users", new List<Dictionary<string, object>>() { user });
            dataAdapter.AddData("DocumentTypes", new List<Dictionary<string, object>>() { docType });
            dataAdapter.AddData("FileCabinets", new List<Dictionary<string, object>>() { role });

            Assert.IsTrue(dataAdapter.AddData("Documents", new List<Dictionary<string, object>>() { doc }));
        }

        [TestMethod]
        public void DataAdapter_ShouldRetreiveAddedData()
        {
            clearDb();

            Dictionary<string, object> role = new Dictionary<string, object>();
            Dictionary<string, object> user = new Dictionary<string, object>();
            Dictionary<string, object> docType = new Dictionary<string, object>();
            Dictionary<string, object> cab = new Dictionary<string, object>();
            Dictionary<string, object> doc = new Dictionary<string, object>();

            generateMockData(role, user, docType, cab, doc);

            IDataAdapter dataAdapter = new DatabaseDataAdapter(testDbConnectionString);
            dataAdapter.AddData("Roles", new List<Dictionary<string, object>>() { role });
            dataAdapter.AddData("Users", new List<Dictionary<string, object>>() { user });
            dataAdapter.AddData("DocumentTypes", new List<Dictionary<string, object>>() { docType });
            dataAdapter.AddData("FileCabinets", new List<Dictionary<string, object>>() { role });
            dataAdapter.AddData("Documents", new List<Dictionary<string, object>>() { doc });

            var docs = dataAdapter.GetData("Documents", null);

            Assert.AreEqual(1, docs.Count);
        }

        [TestMethod]
        public void DataAdapter_ShouldDeleteData()
        {
            clearDb();

            Dictionary<string, object> role = new Dictionary<string, object>();
            Dictionary<string, object> user = new Dictionary<string, object>();
            Dictionary<string, object> docType = new Dictionary<string, object>();
            Dictionary<string, object> cab = new Dictionary<string, object>();
            Dictionary<string, object> doc = new Dictionary<string, object>();

            generateMockData(role, user, docType, cab, doc);

            IDataAdapter dataAdapter = new DatabaseDataAdapter(testDbConnectionString);
            dataAdapter.AddData("Roles", new List<Dictionary<string, object>>() { role });
            dataAdapter.AddData("Users", new List<Dictionary<string, object>>() { user });
            dataAdapter.AddData("DocumentTypes", new List<Dictionary<string, object>>() { docType });
            dataAdapter.AddData("FileCabinets", new List<Dictionary<string, object>>() { role });

            dataAdapter.AddData("Documents", new List<Dictionary<string, object>>() { doc });

            var dq = new DataQuery();
            dq.AddQuery("id", doc["id"]);
            dataAdapter.DeleteData("Documents", dq);

            var docs = dataAdapter.GetData("Documents", null);

            Assert.AreEqual(0, docs.Count);

        }

        [TestMethod]
        public void DataAdapter_ShouldModifyData()
        {
            clearDb();

            Dictionary<string, object> role = new Dictionary<string, object>();
            Dictionary<string, object> user = new Dictionary<string, object>();
            Dictionary<string, object> docType = new Dictionary<string, object>();
            Dictionary<string, object> cab = new Dictionary<string, object>();
            Dictionary<string, object> doc = new Dictionary<string, object>();

            generateMockData(role, user, docType, cab, doc);

            IDataAdapter dataAdapter = new DatabaseDataAdapter(testDbConnectionString);
            dataAdapter.AddData("Roles", new List<Dictionary<string, object>>() { role });
            dataAdapter.AddData("Users", new List<Dictionary<string, object>>() { user });
            dataAdapter.AddData("DocumentTypes", new List<Dictionary<string, object>>() { docType });
            dataAdapter.AddData("FileCabinets", new List<Dictionary<string, object>>() { role });

            dataAdapter.AddData("Documents", new List<Dictionary<string, object>>() { doc });
            doc["name"] = "'new name'";
            var dq = new DataQuery();
            dq.AddQuery("id", doc["id"]);
            dataAdapter.UpdateData("Documents", doc, dq);


            var docs = dataAdapter.GetData("Documents", null);
            Assert.AreEqual(docs[0]["name"], "new name");

        }





    }

}
