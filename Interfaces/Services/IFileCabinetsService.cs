﻿using Interfaces.Data;
using Models;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IFileCabinetsService
    {
        List<FileCabinet> GetFileCabinets(IDataQuery query = null);
        FileCabinet UpdateFileCabinet(FileCabinet updatedFileCabinet);
        bool AddFileCabinet(FileCabinet fileCabinet);
        int GetCount(IDataQuery query);
        void DeleteFileCabinet(string guid);
    }
}
