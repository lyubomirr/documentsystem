﻿using Interfaces.Data;
using System.Collections.Generic;
using Models;

namespace Interfaces.Services
{
    public interface IDocumentsService
    {
        List<Document> GetDocuments(IDataQuery query = null);
        Document GetDocumentById(string guid);
        Document UpdateDocument(Document updatedDoc);
        void DeleteDocument(Document doc);
        void DeleteDocuments(IEnumerable<string> guids);
        void ArchivateDocument(Document doc);
        void ArchivateDocument(string guid);
        void RestoreArchivated(IEnumerable<string> guids);
        void RestoreAllArchivated();  
        void RestoreDeleted(IEnumerable<string> guids);
        void RestoreAllDeleted();
        int GetDocumentCount(IDataQuery dq);
        bool AddDocument(Document doc);
    }
}
