﻿using Interfaces.Data;
using Models;
using System.Collections.Generic;


namespace Interfaces.Services
{
    public interface IRolesService
    {
        List<Role> GetRoles(IDataQuery query = null);
    }
}
