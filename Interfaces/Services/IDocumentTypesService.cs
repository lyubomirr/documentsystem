﻿using Interfaces.Data;
using Models;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IDocumentTypesService
    {
        List<DocumentType> GetDocumentTypes(IDataQuery query = null);
    }
}
