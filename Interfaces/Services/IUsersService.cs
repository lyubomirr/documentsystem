﻿using Interfaces.Data;
using Models;
using System.Collections.Generic;


namespace Interfaces.Services
{
    public interface IUsersService
    {
        List<User> GetUsers(IDataQuery query = null);
        User GetUserById(string guid);
    }
}
