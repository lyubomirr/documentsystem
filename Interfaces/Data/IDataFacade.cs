﻿using Configuration;
using System.Collections.Generic;

namespace Interfaces.Data
{
    public interface IDataFacade
    {
        List<ModelT> GetTableData<ModelT>(IDataQuery query, string tableName) where ModelT : class, new();

        List<ModelT> GetJoinedTableData<ModelT, JoinT>(IDataQuery query,
            string tableName, string populatedProperty) where ModelT : class, new() where JoinT : class, new();

        bool DeleteObject(IDataQuery query, string tableName);
        bool ModifyObject<ModelT>(ModelT modified, IDataQuery query, string tableName) where ModelT: class;
        bool AddObject<ModelT>(ModelT newObject, string tableName) where ModelT : class;
        int GetTableRowCount(string tableName, IDataQuery dq = null);
    }
}
