﻿using System.Collections.Generic;

namespace Interfaces.Data
{
    public interface IDataAdapter
    {
        void SetSource(string connectionString);
        bool AddData(string tableName, List<Dictionary<string, object>> data);
        List<Dictionary<string, object>> GetData(string tableName, IDataQuery query);
        bool UpdateData(string tableName, Dictionary<string, object> newData, IDataQuery query);
        bool DeleteData(string tableName, IDataQuery query);
        int GetTableRowCount(string tableName, IDataQuery query);
    }

}
