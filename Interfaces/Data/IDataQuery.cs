﻿namespace Interfaces.Data
{
    public interface IDataQuery
    {
        IDataQuery Init(string tableName);
        IDataQuery AddQuery(string property, string value);
        IDataQuery AddQuery(string property, object value);
        IDataQuery SortBy(string sortProperty, bool? isAsc = true);
        IDataQuery GetPage(int? pageNumber, int? elemPerPage = null);
        IDataQuery SearchBy(string property, object value);
        IDataQuery SearchBy(string[] properties, object value);
        IDataQuery Join(string joinTable, string foreignProperty, string primaryProperty);
        string GetJoinedTable();
        string ToSql();
    }
}
