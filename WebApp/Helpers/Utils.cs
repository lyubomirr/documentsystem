﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Helpers
{
    public static class Utils
    {
        public static bool isAuthenticated(HttpRequestBase request, HttpSessionStateBase session)
        {
            return (
                request.Cookies.Get("AuthToken") != null && session["AuthToken"] != null
                && request.Cookies.Get("AuthToken").Value.ToString() == session["AuthToken"].ToString()
                );
        }
    }
}