﻿using System;

namespace WebApp.Helpers
{
    public class UploadedFileInfo
    {
        public Guid Guid { get; set; }
        public Guid FileCabinetGuid { get; set; }
        public string FileName { get; set; }
    }
}