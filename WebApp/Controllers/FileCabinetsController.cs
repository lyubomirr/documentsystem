﻿using Interfaces.Data;
using Interfaces.Services;
using Ionic.Zip;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using WebApp.Authorize;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class FileCabinetsController : Controller
    {
        private IFileCabinetsService _fileCabinetService;
        private IDocumentsService _documentsService;
        private IDataQuery _dq;

        public FileCabinetsController(IFileCabinetsService fileCabinetsService, IDocumentsService documentsService,
            IDataQuery dq)
        {
            _fileCabinetService = fileCabinetsService;
            _documentsService = documentsService;
            _dq = dq;
        }

        public ActionResult Index(string search, int? pageNumber, int? elemPerPage, string sortProperty, bool? isAsc)
        {
            _dq.Init("FileCabinets")
                .AddQuery("Deleted", 0)
                .SearchBy("Name", search)
                .GetPage(pageNumber, elemPerPage)
                .SortBy(sortProperty, isAsc);

            var fcs = _fileCabinetService.GetFileCabinets(_dq);
            return Json(fcs, JsonRequestBehavior.AllowGet);
        }

        [Route("filecabinets/count/")]
        public ActionResult GetCount(string search)
        {
            _dq.Init("FileCabinets")
                .AddQuery("Deleted", 0)
                .SearchBy("Name", search);

            int count = _fileCabinetService.GetCount(_dq);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeNotGuest]
        [HttpPost]
        public ActionResult Add(FileCabinet fileCabinet)
        {
            fileCabinet.Guid = Guid.NewGuid();
            var fcDirectoryPath = Path.Combine("/Uploads/", fileCabinet.Guid.ToString());
            Directory.CreateDirectory(Server.MapPath(fcDirectoryPath));

             _fileCabinetService.AddFileCabinet(fileCabinet);
             return Json(fileCabinet);
        }

        [AuthorizeNotGuest]
        [Route("filecabinets/delete/{guid}")]
        public ActionResult Delete(string guid)
        {
            _dq.Init("Documents")
                .AddQuery("FileCabinetId", guid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);

            var docsInFc = _documentsService.GetDocuments(_dq);
            foreach(var doc in docsInFc)
            {
                _documentsService.DeleteDocument(doc);
            }

            _fileCabinetService.DeleteFileCabinet(guid);

            return new EmptyResult();     
        }

        [AuthorizeNotGuest]
        [Route("filecabinets/archivate/{guid}")]
        public ActionResult Archivate(string guid)
        {
            _dq.Init("Documents")
                .AddQuery("FileCabinetId", guid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);

            var docsInFc = _documentsService.GetDocuments(_dq);
            foreach (var doc in docsInFc)
            {
                _documentsService.ArchivateDocument(doc);
            }

            return new EmptyResult();
        }

        [AuthorizeNotGuest]
        [Route("filecabinets/update")]
        [HttpPost]
        public void Update(FileCabinet updated)
        {
            _fileCabinetService.UpdateFileCabinet(updated);
        }

        [AuthorizeNotGuest]
        [Route("filecabinets/export/{guid}")]
        public ActionResult Export(string guid, string filename)
        {
            _dq.Init("Documents")
                .AddQuery("FileCabinetId", guid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);

            var docsInFc = _documentsService.GetDocuments(_dq);
            if(docsInFc.Count == 0)
            {
                return new EmptyResult();
            }

            IEnumerable<string> guids = docsInFc.Select(doc => doc.Guid.ToString());
            string guidsJoined = String.Join(",", guids);

            return RedirectToAction("ExportFiles", "Documents", new { guids = guidsJoined, filename });
        }

        //[AuthorizeNotGuest]
        //[HttpPost]
        //[Route("filecabinets/import/{guid}")]
        //public ActionResult Import(string guid)
        //{
        //    if(Request.Files.Count == 0)
        //    {
        //        return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
        //    }

        //    var importedDocs = new List<Document>();

        //    ZipFile zip = ZipFile.Read(Request.Files[0].InputStream);
        //    foreach(var entry in zip.Entries)
        //    {
        //        entry.Extract(Server.MapPath("/Uploads/"));

        //        if(Path.GetFileName(entry.FileName) == "data.xml")
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(Document));
        //            var xmlFilePath = Server.MapPath(Path.Combine("/Uploads/", entry.FileName));

        //            using (FileStream fs = new FileStream(xmlFilePath, FileMode.Open))
        //            {
        //                Document deserialized = (Document)serializer.Deserialize(fs);
        //                deserialized.FileCabinetId = Guid.Parse(guid);
        //                deserialized.Owner = _usersService.GetUserById(deserialized.OwnerId.ToString());

        //                _documentsService.AddDocument(deserialized);
        //                importedDocs.Add(deserialized);
        //            }
        //        }
        //    }

        //    return Json(importedDocs);
        //}


    }
}