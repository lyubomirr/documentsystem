﻿using System;
using System.Web.Mvc;
using WebApp.Authorize;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(Utils.isAuthenticated(Request, Session))
            {
                return RedirectToAction("Index", "App");
            }

            return View();
        }

        [AuthorizeUser]
        [AuthorizeNotGuest]
        public ActionResult GetRandomGuid()
        {
            return Json(Guid.NewGuid(), JsonRequestBehavior.AllowGet);
        }
    }
}