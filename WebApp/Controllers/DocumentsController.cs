﻿using Ionic.Zip;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using WebApp.Authorize;
using WebApp.Helpers;
using Models;
using Interfaces.Services;
using Interfaces.Data;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class DocumentsController : Controller
    {
        private IDocumentsService _documentsService;
        IUsersService _usersService;
        private IDataQuery _dq;
        private string[] _searchProperties = { "Name", "Description", "Subject", "Contact", "Company", "RegDocNumber" };

        public DocumentsController(IDocumentsService documentsService, IUsersService usersService, IDataQuery dq)
        {
            _documentsService = documentsService;
            _usersService = usersService;
            _dq = dq;
        }

        [Route("documents/{fcGuid}")]
        public ActionResult Index(string fcGuid, string searchType, string search, int? pageNumber, int? elemPerPage,
            string sortProperty, bool? isAsc
            )
        {
            _dq.Init("Documents")
                .Join("Users", "OwnerId", "Guid")
                .AddQuery("FileCabinetId", fcGuid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search)
                .GetPage(pageNumber, elemPerPage)
                .SortBy(sortProperty, isAsc);

            var documentsInFc = _documentsService.GetDocuments(_dq);
            return Json(documentsInFc, JsonRequestBehavior.AllowGet);
        }

        [Route("documents/count/{fcGuid}")]
        public ActionResult GetCount(string fcGuid, string searchType, string search)
        {
            _dq.Init("Documents")
                .AddQuery("FileCabinetId", fcGuid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search);

            int count = _documentsService.GetDocumentCount(_dq);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        [Route("documents/deleted/count")]
        public ActionResult GetDeletedCount(string searchType, string search)
        {
            _dq.Init("Documents")
                .AddQuery("Deleted", 1)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search);
        
            int count = _documentsService.GetDocumentCount(_dq);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        [Route("documents/archive/count")]
        public ActionResult GetArchiveCount(string searchType, string search)
        {
            _dq.Init("Documents")
                .AddQuery("Archived", 1)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search);

            int count = _documentsService.GetDocumentCount(_dq);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        //[AuthorizeNotGuest]
        //[HttpPost]
        //[Route("documents/uploadfile")]
        //public ActionResult UploadFile()
        //{
        //    if (Request.Files.Count > 0)
        //    {
        //        foreach (string file in Request.Files)
        //        {
        //            var _file = Request.Files[file];
        //            string filename = Path.GetFileName(_file.FileName);

        //            UploadedFileInfo fileInfo = new UploadedFileInfo();
        //            fileInfo.DocumentGuid = Guid.NewGuid();
        //            fileInfo.FilePath = $"/Uploads/{fileInfo.DocumentGuid}/{filename}"; 

        //            string directoryPath = Path.Combine(Server.MapPath("~/Uploads/"), fileInfo.DocumentGuid.ToString());
        //            Directory.CreateDirectory(directoryPath);
        //            string filepath = Path.Combine(directoryPath, filename);
        //            _file.SaveAs(filepath);

        //            return Json(fileInfo);
        //        }
        //    }
        //    return new EmptyResult();
        //}

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/uploadchunks/{guid}")]
        public ActionResult UploadChunks(string guid)
        {
            try
            {
                var directory = Directory.CreateDirectory(Path.Combine(Server.MapPath("~/Uploads/Temp"), guid));
                var chunk = Request.Files[0];
                string filePath = Path.Combine(directory.FullName, Path.GetRandomFileName());
                chunk.SaveAs(filePath);

                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception)
            {
                Directory.Delete(Path.Combine(Server.MapPath("~/Uploads/Temp"), guid));
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/completeupload/")]
        public ActionResult CompleteUpload(UploadedFileInfo fileInfo)
        {
            string newFileDirectoryPath = Path.Combine(Server.MapPath("~/Uploads/"),
            fileInfo.FileCabinetGuid.ToString(), fileInfo.Guid.ToString());

            string tempFilesPath = Path.Combine(Server.MapPath("~/Uploads/Temp/"), fileInfo.Guid.ToString());
            string newFilePath = Path.Combine(newFileDirectoryPath, fileInfo.FileName);

            Directory.CreateDirectory(newFileDirectoryPath);
            ConstructUploadedFile(fileInfo, tempFilesPath, newFilePath);
            Directory.Delete(tempFilesPath, true);

            string newFileRelativePath = Path.Combine("/Uploads/", fileInfo.FileCabinetGuid.ToString(),
                fileInfo.Guid.ToString(), fileInfo.FileName);

            return Json(newFileRelativePath);
        }

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/completeimport/")]
        public ActionResult CompleteImport(UploadedFileInfo importInfo)
        {
            string tempFilesPath = Path.Combine(Server.MapPath("~/Uploads/Temp/"), importInfo.Guid.ToString());
            string newFilePath = Path.Combine(tempFilesPath, importInfo.FileName);

            ConstructUploadedFile(importInfo, tempFilesPath, newFilePath);

            var importedDocs = new List<Document>();
            ZipFile zip = new ZipFile();

            try
            {
                zip = ZipFile.Read(newFilePath);
            }
            catch
            {
                Directory.Delete(Path.GetDirectoryName(newFilePath), true);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            var directories = GetZipDirectories(zip);
            Dictionary<string, Guid> mapNewGuids = new Dictionary<string, Guid>();
            foreach(var directory in directories)
            {
                mapNewGuids[directory] = Guid.NewGuid();
            }

            foreach (var entry in zip.Entries.ToList())
            {
                string onlyFileName = Path.GetFileName(entry.FileName);
                string entryDirectory = Path.GetDirectoryName(entry.FileName);

                entry.FileName = Path.Combine(mapNewGuids[entryDirectory].ToString(), onlyFileName);
                entry.Extract(Server.MapPath($"/Uploads/{importInfo.FileCabinetGuid}"));

                if (onlyFileName == "data.xml")
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Document));
                    var xmlFilePath = Server.MapPath(Path.Combine("/Uploads/",
                        importInfo.FileCabinetGuid.ToString(), entry.FileName));

                    using (FileStream fs = new FileStream(xmlFilePath, FileMode.Open))
                    {
                        Document deserialized = (Document)serializer.Deserialize(fs);

                        string fileName = Path.GetFileName(deserialized.FilePath);

                        deserialized.Guid = mapNewGuids[entryDirectory];
                        deserialized.FileCabinetId = importInfo.FileCabinetGuid;
                        deserialized.FilePath = Path.Combine("/Uploads", importInfo.FileCabinetGuid.ToString(),
                            deserialized.Guid.ToString(), fileName);

                        deserialized.Owner = _usersService.GetUserById(deserialized.OwnerId.ToString());

                        _documentsService.AddDocument(deserialized);
                        importedDocs.Add(deserialized);
                    }
                }
            }

            zip.Dispose();

            Directory.Delete(Path.GetDirectoryName(newFilePath), true);
            return Json(importedDocs);
        }   

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/add")]
        public ActionResult Add(Document doc)
        {
            doc.StoreDate = DateTime.Now;
            doc.OwnerId = Guid.Parse(Session["UserGuid"].ToString());
            doc.Owner = _usersService.GetUserById(doc.OwnerId.ToString());

            SaveDocumentMetaData(doc);
            _documentsService.AddDocument(doc);

            return Json(doc);
        }

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/update")]
        public void Update(Document doc)
        {
            doc.LastModifiedDate = DateTime.Now;
            doc.LastModifiedUser = Guid.Parse(Session["UserGuid"].ToString());

            SaveDocumentMetaData(doc);
            _documentsService.UpdateDocument(doc);
        }

        [AuthorizeNotGuest]
        [Route("documents/delete")]
        public void Delete(string guids)
        {
            var guidsList = guids.Split(',');
            _documentsService.DeleteDocuments(guidsList);
        }

        [AuthorizeNotGuest]
        [Route("documents/archivate/{guid}")]
        public void Archivate(string guid)
        {
            _documentsService.ArchivateDocument(guid);
        }


        [Route("documents/getfile/{guid}")]
        public ActionResult GetFile(string guid, bool? isFileView)
        {
            var doc = _documentsService.GetDocumentById(guid);
            if(doc == null)
            {
                return new EmptyResult();
            }

            string fileName = Path.GetFileName(doc.FilePath);           
            if(isFileView != null)
            {
                return (bool)isFileView ? File(doc.FilePath, MimeTypes.GetMimeType(fileName))
                    : File(doc.FilePath, MimeTypes.GetMimeType(fileName), fileName);
            }

            return File(doc.FilePath, MimeTypes.GetMimeType(fileName), fileName);
        }
     
        [AuthorizeNotGuest]
        [Route("documents/exportdocuments/")]
        public ActionResult ExportFiles(string filename, string guids)
        {
            var docs = _documentsService.GetDocuments();
            if (docs.Count > 0)
            {
                using (ZipFile zip = new ZipFile())
                {
                    string fullFilename = String.IsNullOrEmpty(filename)
                        ? $"{Path.GetFileNameWithoutExtension(Server.MapPath(docs[0].FilePath))}.{DateTime.Now.ToShortDateString()}"
                        : filename;

                    var guidsList = guids.Split(',');

                    foreach (var doc in docs)
                    {
                        if (guidsList.Contains(doc.Guid.ToString()))
                        {
                            var fileDirectory = Path.GetDirectoryName(doc.FilePath);
                            var dataFilePath = Path.Combine(Server.MapPath(fileDirectory), "data.xml");


                            if(System.IO.File.Exists(dataFilePath))
                            {
                                zip.AddFile(dataFilePath, doc.Guid.ToString());
                            }
                            if(System.IO.File.Exists(Server.MapPath(doc.FilePath)))
                            {
                                zip.AddFile(Server.MapPath(doc.FilePath), doc.Guid.ToString());
                            }                           
                        }
                    }

                    MemoryStream stream = new MemoryStream();
                    zip.Save(stream);

                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/zip", fullFilename + ".zip");
                }
            }

            return new EmptyResult();
        }

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/copy")]
        public ActionResult Copy(Document newDocument)
        {
            newDocument.CopyFromDocumentId = newDocument.Guid;
            newDocument.Guid = Guid.NewGuid();
            newDocument.StoreDate = DateTime.Now;

            string oldPath = newDocument.FilePath;
            string fileName = Path.GetFileName(oldPath);

            newDocument.FilePath = Path.Combine("/Uploads/", newDocument.FileCabinetId.ToString(),
                newDocument.Guid.ToString(), fileName);

            string directoryPath = Path.Combine(Server.MapPath("~/Uploads/"), 
                newDocument.FileCabinetId.ToString(), newDocument.Guid.ToString());

            Directory.CreateDirectory(directoryPath);

            System.IO.File.Copy(Server.MapPath(oldPath), Server.MapPath(newDocument.FilePath));

            var oldDirectoryPath = Server.MapPath(Path.Combine(Path.GetDirectoryName(oldPath)));
            System.IO.File.Copy(Path.Combine(oldDirectoryPath, "data.xml"), Path.Combine(directoryPath, "data.xml"));

            SaveDocumentMetaData(newDocument);
            _documentsService.AddDocument(newDocument);
            return Json(newDocument);
        }

        [AuthorizeNotGuest]
        [Route("documents/archive")]
        public ActionResult GetArchive(string searchType, string search, int? pageNumber, int? elemPerPage, 
            string sortProperty, bool? isAsc)
        {
            _dq.Init("Documents")
                .Join("Users", "OwnerId", "Guid")
                .AddQuery("Archived", 1)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search)
                .GetPage(pageNumber, elemPerPage)
                .SortBy(sortProperty, isAsc);

            var docs = _documentsService.GetDocuments(_dq);
            return Json(docs, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeAdmin]
        [Route("documents/deleted")]
        public ActionResult GetDeleted(string searchType, string search, int? pageNumber, int? elemPerPage,
            string sortProperty, bool? isAsc)
        {
            _dq.Init("Documents")
                .Join("Users", "OwnerId", "Guid")
                .AddQuery("Deleted", 1)
                .AddQuery("TypeId", searchType)
                .SearchBy(_searchProperties, search)
                .GetPage(pageNumber, elemPerPage)
                .SortBy(sortProperty, isAsc);

            var docs = _documentsService.GetDocuments(_dq);
            return Json(docs, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeAdmin]
        [Route("documents/restorearchive")]
        public void RestoreArchive(string guids, bool? all)
        {
            if(all == true)
            {
                _documentsService.RestoreAllArchivated();
            }
            else
            {
                var guidsList = guids.Split(',');
                _documentsService.RestoreArchivated(guidsList);
            }
        }

        [AuthorizeAdmin]
        [Route("documents/restoredeleted")]
        public void RestoreDeleted(string guids, bool? all)
        {
            if(all == true)
            {
                _documentsService.RestoreAllDeleted();
            }
            else
            {
                var guidsList = guids.Split(',');
                _documentsService.RestoreDeleted(guidsList);              
            }
        }        

        private void AppendFile(FileStream master, FileStream partial)
        {
            byte[] partialContent = new byte[partial.Length];
            partial.Read(partialContent, 0, partialContent.Length); 
            master.Write(partialContent, 0, partialContent.Length);
        }
        
        private void SaveDocumentMetaData(Document doc)
        {
            var fileDirectory = Path.GetDirectoryName(doc.FilePath);
            var metaDataFilePath = Path.Combine(Server.MapPath(fileDirectory), "data.xml");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Document));
            using (FileStream file = new FileStream(metaDataFilePath, FileMode.Create))
            {
                xmlSerializer.Serialize(file, doc);
            }
        }

        private void ConstructUploadedFile(UploadedFileInfo fileInfo, string tempFilesPath, string newFilePath)
        {
            DirectoryInfo tempFilesDirectoryInfo = new DirectoryInfo(tempFilesPath);
            var tempFiles = tempFilesDirectoryInfo.GetFiles().OrderBy(file => file.CreationTimeUtc).ToArray();

            using (FileStream newFile = new FileStream(newFilePath, FileMode.Append))
            {
                foreach (var tempFile in tempFiles)
                {
                    using (FileStream tempFileStream = new FileStream(tempFile.FullName, FileMode.Open))
                    {
                        AppendFile(newFile, tempFileStream);
                    }
                    System.IO.File.Delete(tempFile.FullName);
                }
            }
        }

        private List<string> GetZipDirectories(ZipFile zip)
        {
            List<string> directories = new List<string>();
            foreach (var entry in zip.Entries)
            {
                directories.Add(Path.GetDirectoryName(entry.FileName));
            }
            return directories.Select(dir => dir).Distinct().ToList();
        }
    }
}