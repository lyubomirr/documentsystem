﻿using Interfaces.Services;
using System.Web.Mvc;
using WebApp.Authorize;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class RolesController : Controller
    {
        private IRolesService _rolesService;

        public RolesController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }

        public ActionResult Index()
        {
            var roles = _rolesService.GetRoles();

            return Json(roles, JsonRequestBehavior.AllowGet);
        }
    }
}