﻿using Interfaces.Data;
using Interfaces.Services;
using System.Web.Mvc;
using WebApp.Authorize;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class DocumentTypesController : Controller
    {
        private IDocumentTypesService _documentTypesService;
        private IDataQuery _dq;

        public DocumentTypesController(IDocumentTypesService documentTypesService, IDataQuery dq)
        {
            _documentTypesService = documentTypesService;
            _dq = dq;
        }

        public ActionResult Index()
        {
            var docTypes = _documentTypesService.GetDocumentTypes();
            return Json(docTypes, JsonRequestBehavior.AllowGet);
        }
    }
}