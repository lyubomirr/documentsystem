﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Authorize;
using WebApp.Helpers;

namespace WebApp.Controllers
{
    public class AppController : Controller
    {
        // GET: App
        public ActionResult Index()
        {
            if(Utils.isAuthenticated(Request, Session))
            {
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        [AuthorizeUser]
        public ActionResult GetCurrentResources()
        {
            return Json(
                Resources.Data.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true)
                .OfType<DictionaryEntry>().ToDictionary(el => el.Key.ToString(), el => el.Value.ToString()), JsonRequestBehavior.AllowGet);
        }
    }
}