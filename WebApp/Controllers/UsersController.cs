﻿using System.Web.Mvc;
using System.Text;
using System.Web;
using System;
using WebApp.Authorize;
using WebApp.Helpers;
using System.Security.Cryptography;
using Interfaces.Services;
using Models;
using Interfaces.Data;

namespace WebApp.Controllers
{
    public class UsersController : Controller
    {
        private IUsersService _usersService;
        private IRolesService _rolesService;
        private IDataQuery _dq;

        public UsersController(IUsersService usersService, IRolesService rolesService, IDataQuery dq)
        {
            _usersService = usersService;
            _rolesService = rolesService;
            _dq = dq;
        }

        private string HashText(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        private void SaveCredentials(Guid userGuid)
        {
            string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            Session["AuthToken"] = token;
            Session["UserGuid"] = userGuid;

            _dq.Init("Users")
                .AddQuery("Guid", userGuid.ToString());

            var user = _usersService.GetUsers(_dq)[0];

            _dq.Init("Roles")
                .AddQuery("Guid", user.RoleId.ToString());

            var role = _rolesService.GetRoles(_dq)[0];
            Session["Role"] = role.Name;

            HttpCookie authTokenCookie = new HttpCookie("AuthToken");
            authTokenCookie.Value = token;
            authTokenCookie.Expires = DateTime.Now.AddHours(8);

            HttpCookie userGuidCookie = new HttpCookie("UserGuid");
            userGuidCookie.Value = userGuid.ToString();
            userGuidCookie.Expires = DateTime.Now.AddHours(8);

            Response.Cookies.Add(authTokenCookie);
            Response.Cookies.Add(userGuidCookie);            
        }

        [AuthorizeUser]
        [Route("users/getusers/{guid?}")]
        public ActionResult GetUsers(string guid)
        {
            if (String.IsNullOrEmpty(guid))
            {
                var users = _usersService.GetUsers();
                return Json(users, JsonRequestBehavior.AllowGet);
            }
            else
            {             
                var user = _usersService.GetUserById(guid);
                return Json(user, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult Login(User loggingUser)
        {
            if(!ModelState.IsValid)
            {
                TempData["ErrorValue"] = "Invalid data! ";
                return RedirectToAction("Index", "Home");
            }

            Hashing hashing = new Hashing(new SHA256Managed());
            
            _dq.Init("Users")
                .AddQuery("Username", loggingUser.Username);

            var users = _usersService.GetUsers(_dq);

            if(users.Count == 0)
            {
                TempData["ErrorValue"] = "No such user! ";
                return RedirectToAction("Index", "Home");
            }

            if (hashing.VerifyHash(loggingUser.Password, users[0].Password, users[0].Salt))
            {                    
                SaveCredentials(users[0].Guid);
                return RedirectToAction("Index", "App"); 
            }

            TempData["ErrorValue"] = "Wrong password! ";
            return RedirectToAction("Index", "Home");        
        }

        [AuthorizeUser]
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}