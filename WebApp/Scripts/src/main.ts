﻿/// <reference path="misc/globals.d.ts" />

$(document).ready(function () {

    App = new Global.Application();
    App.Service = new Services.ApplicationService();
    App.Utils = new Services.Utils();
    App.Config = new Services.Config();
    $('[data-toggle="tooltip"]').tooltip();
    $.when(App.Service.init())
        .done(() => {
            var vm = new Vms.ApplicationVm();
            ko.applyBindings(vm, $("#app-root")[0]);
        });
});