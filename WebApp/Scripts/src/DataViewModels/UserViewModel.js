var DataVms;
(function (DataVms) {
    var UserViewModel = /** @class */ (function () {
        function UserViewModel(model) {
            this.Guid = "";
            this.Username = "";
            this.Role = new Models.Role();
            if (model) {
                this.Guid = model.Guid;
                this.Username = model.Username;
                this.Role = App.Service.dataMapper.getRole(model.RoleId);
            }
        }
        return UserViewModel;
    }());
    DataVms.UserViewModel = UserViewModel;
})(DataVms || (DataVms = {}));
//# sourceMappingURL=UserViewModel.js.map