﻿ namespace DataVms {
    export class DocumentViewModel {
        private model: Models.Document = new Models.Document();

        public Guid: string = "";
        public Name: KnockoutObservable<string> = ko.observable("");
        public Description: KnockoutObservable<string> = ko.observable("");
        public Type: KnockoutObservable<Models.DocumentType> = ko.observable(new Models.DocumentType());
        public StoreDate: KnockoutObservable<Date> = ko.observable(new Date());
        public Amount: KnockoutObservable<number> = ko.observable();
        public Vat: KnockoutObservable<number> = ko.observable();
        public TotalAmount: KnockoutObservable<number> = ko.computed(() => {
            return this.Amount() * (1 + this.Vat() / 100);
        });
        public FilePath: KnockoutObservable<string> = ko.observable("");
        public Subject: KnockoutObservable<string> = ko.observable("");
        public Owner: KnockoutObservable<string> = ko.observable("");
        public RegDocNumber: KnockoutObservable<string> = ko.observable("");
        public Contact: KnockoutObservable<string> = ko.observable("");
        public Company: KnockoutObservable<string> = ko.observable("");

        public isChecked: KnockoutObservable<boolean> = ko.observable(false);

        constructor(model?: Models.Document) {
            if (model) {
                this.model = model;
                this.Guid = model.Guid;
                this.Name(model.Name);
                this.Description(model.Description);
                this.Type(App.Service.dataMapper.getDocumentType(model.TypeId));
                this.StoreDate(App.Utils.parseJsonDate(model.StoreDate));
                this.Amount(model.Amount);
                this.Vat(model.Vat);
                this.FilePath(model.FilePath);
                this.Subject(model.Subject);
                this.Owner(model.Owner.Username);
                this.RegDocNumber(model.RegDocNumber);
                this.Contact(model.Contact);
                this.Company(model.Company);
            }
        }
        

        getModel(): Models.Document {
            this.model.Name = this.Name();
            this.model.Description = this.Description();
            this.model.Amount = this.Amount();
            this.model.Vat = this.Vat();
            this.model.TotalAmount = this.TotalAmount();
            this.model.Subject = this.Subject();
            this.model.RegDocNumber = this.RegDocNumber();
            this.model.Contact = this.Contact();
            this.model.Company = this.Company();
            this.model.StoreDate = this.StoreDate().toLocaleDateString();
            if (this.model.LastModifiedDate) {
                this.model.LastModifiedDate = App.Utils.parseJsonDate(this.model.LastModifiedDate).toLocaleDateString();
            }

            return this.model;
        }

    }
}