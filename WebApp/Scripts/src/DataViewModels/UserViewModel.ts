﻿namespace DataVms {
    export class UserViewModel {
        public Guid: string = "";
        public Username: string = "";
        public Role: Models.Role = new Models.Role();

        constructor(model?: Models.User) {
            if (model) {
                this.Guid = model.Guid;
                this.Username = model.Username;
                this.Role = App.Service.dataMapper.getRole(model.RoleId);
            }
        }
    }
}