﻿namespace Vms {
    export class FileCabinetsVm {
        private currentUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable(new DataVms.UserViewModel(App.Service.currentUser));

        //General
        private fcModalData: KnockoutObservable<DataVms.FileCabinetViewModel> = ko.observable(new DataVms.FileCabinetViewModel());
        public fileCabinets: KnockoutObservableArray<DataVms.FileCabinetViewModel> = ko.observableArray(new Array<DataVms.FileCabinetViewModel>());
        public isEdit: KnockoutObservable<boolean> = ko.observable(false);

        //Authorization
        private shouldBeActive: KnockoutObservable<boolean>;
        private isAdmin: KnockoutObservable<boolean>;


        //Pagination
        public currentPage: KnockoutObservable<number> = ko.observable(1);
        public pageElements: KnockoutObservable<number> = ko.observable(5);
        public maxPageNumber: KnockoutComputed<number>;

        //Search
        private searchFieldValue: KnockoutObservable<string> = ko.observable("");

        //Sort
        private isAscToggle: KnockoutObservable<boolean> = ko.observable(true);
        private sortProperty: KnockoutObservable<string> = ko.observable("Name");
        private fcQuery: KnockoutComputed<string>;


        constructor() {
            this.shouldBeActive = ko.computed(() => {
                return this.currentUser().Role.Name !== UserRoles.Guest;
            });
            this.isAdmin = ko.computed(() => {
                return this.currentUser().Role.Name === UserRoles.Admin;
            });
            this.fcQuery = ko.computed(() => {
                return "?sortProperty=" + this.sortProperty() + "&isAsc=" + this.isAscToggle()
                    + "&pageNumber=" + this.currentPage() + "&elemPerPage=" + this.pageElements()
                    + "&search=" + this.searchFieldValue();
            });
            this.maxPageNumber = ko.computed(() => {
                return Math.ceil(App.Config.currentDataCount() / this.pageElements());
            });
            this.fcQuery.subscribe(() => {
                let query = "?search=" + this.searchFieldValue();
                App.Service.connManager.invokeGet<number>(App.Service.endpoints.getFileCabinetCount, query)
                    .then((count: number) => {
                        App.Config.currentDataCount(count);
                        this.loadFileCabinets();
                    });
            });

            this.searchFieldValue.subscribe(() => {
                this.currentPage(1);
            });

        }

        public loadFileCabinets(): void {
            App.Service.connManager.invokeGet<Models.FileCabinet[]>(App.Service.endpoints.getFileCabinets, this.fcQuery())
                .then((data: Models.FileCabinet[]) => {
                    let parsed: DataVms.FileCabinetViewModel[] = [];
                    for (let fc of data) {
                        parsed.push(new DataVms.FileCabinetViewModel(fc));
                    }
                    this.fileCabinets(parsed);
                });
        }

        private addFileCabinet(): void {
            if (this.isFcModelValid()) {
                App.Service.connManager.invokePost<Models.FileCabinet>(App.Service.endpoints.addFileCabinet, this.fcModalData().toModel())
                    .then((returnedFc: Models.FileCabinet) => {
                        if (this.fileCabinets().length == this.pageElements()) {
                            this.fileCabinets.pop();
                        }
                        this.fileCabinets.unshift(new DataVms.FileCabinetViewModel(returnedFc));
                        this.fcModalData(new DataVms.FileCabinetViewModel());
                        App.Utils.hideModal("#file-cabinet-modal");
                    });
            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private updateFileCabinet(): void {
            if (this.isFcModelValid()) {
                App.Service.connManager.invokePost<Models.FileCabinet>(App.Service.endpoints.updateFileCabinet, this.fcModalData().toModel())
                    .then(() => {
                        for (let el of this.fileCabinets()) {
                            if (el.Guid == this.fcModalData().Guid) {
                                this.fileCabinets.replace(el, this.fcModalData());
                                App.Utils.hideModal("#file-cabinet-modal");
                                break;
                            }
                        };
                    });
            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private getNextPage(): void {
            if (this.currentPage() < this.maxPageNumber()) {
                this.currentPage(this.currentPage() + 1);
            }
        }

        private getPrevPage(): void {
            if (this.currentPage() > 1) {
                this.currentPage(this.currentPage() - 1);
            }
        }

        private isFcModelValid(): boolean {
            return App.Utils.isInputValid(this.fcModalData().Name());
        }

        private sortByProperty(propName: string) {
            if (this.sortProperty() != propName) {
                this.sortProperty(propName);
                this.isAscToggle(true);
            } else {
                this.isAscToggle(!this.isAscToggle());
            }
        }

        private setModalData(fc?: DataVms.FileCabinetViewModel) {
            if (fc) {
                let clone: DataVms.FileCabinetViewModel = new DataVms.FileCabinetViewModel(fc.toModel());
                this.fcModalData(clone);
            } else {
                this.fcModalData(new DataVms.FileCabinetViewModel());
            }
        }

        private openFcModal(fc?: DataVms.FileCabinetViewModel) {
            if (fc) {
                this.isEdit(true);
            } else {
                this.isEdit(false);
            }
            this.setModalData(fc);
        }

    }
}