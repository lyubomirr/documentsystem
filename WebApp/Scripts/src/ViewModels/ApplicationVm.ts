﻿namespace Vms {
    export class ApplicationVm {
        private currentUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable(new DataVms.UserViewModel(App.Service.currentUser));

        private fileCabinetsVm: KnockoutObservable<Vms.FileCabinetsVm> = ko.observable(new Vms.FileCabinetsVm());
        private documentsVm: KnockoutObservable<Vms.DocumentsVm> = ko.observable(null);

        private isFcViewVisible: KnockoutObservable<boolean> = ko.observable(true);
        private modalTitle: KnockoutObservable<String> = ko.observable("");
        private modalText: KnockoutObservable<String> = ko.observable("");
        private modalCb: CallbackFunction = () => { };

        constructor() {
            this.openFcList();
        }

        private openFileCabinet(fileCabinet: DataVms.FileCabinetViewModel): void {
            App.Service.connManager.invokeGet<number>(App.Service.endpoints.getDocumentCount, fileCabinet.Guid)
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.activateDocView();
                    this.documentsVm().setFileCabinet(fileCabinet.toModel());
                });
        }

        private openArchive(): void {
            App.Service.connManager.invokeGet<number>(App.Service.endpoints.getArchivedCount)
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.activateDocView();
                    this.documentsVm().setArchive();
                });
        }

        private openTrash(): void {
            App.Service.connManager.invokeGet<number>(App.Service.endpoints.getDeletedCount)
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.activateDocView();
                    this.documentsVm().setTrash();
                });
        }

        private openFcList(): void {
            App.Service.connManager.invokeGet<number>(App.Service.endpoints.getFileCabinetCount)
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.activateFcView();
                    this.fileCabinetsVm().loadFileCabinets();
                });
        }

        private deleteFileCabinet(caller: DataVms.FileCabinetViewModel): void {
            App.Service.connManager.invokeGet<Models.FileCabinet>(App.Service.endpoints.deleteFileCabinet, caller.Guid)
                .then(() => {
                    this.fileCabinetsVm().fileCabinets.remove(caller);
                });
        }

        private deleteFileCabinetFromDocView(caller: DataVms.FileCabinetViewModel): void {
            App.Service.connManager.invokeGet<Models.FileCabinet>(App.Service.endpoints.deleteFileCabinet, caller.Guid)
                .then(() => {
                    this.openFcList();
                    this.fileCabinetsVm().fileCabinets.remove(caller);
                });
        }

        private openConfirmModal(titleKey: string, textKey: string, cb: CallbackFunction, name?: string): void {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            } else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        }

        private activateFcView(): void {
            this.documentsVm(null);
            this.fileCabinetsVm(new FileCabinetsVm());
        }

        private activateDocView(): void {
            this.fileCabinetsVm(null);
            this.documentsVm(new DocumentsVm());
        }
    }
}