var Vms;
(function (Vms) {
    var FileCabinetsVm = /** @class */ (function () {
        function FileCabinetsVm() {
            var _this = this;
            this.currentUser = ko.observable(new DataVms.UserViewModel(App.Service.currentUser));
            //General
            this.fcModalData = ko.observable(new DataVms.FileCabinetViewModel());
            this.fileCabinets = ko.observableArray(new Array());
            this.isEdit = ko.observable(false);
            //Pagination
            this.currentPage = ko.observable(1);
            this.pageElements = ko.observable(5);
            //Search
            this.searchFieldValue = ko.observable("");
            //Sort
            this.isAscToggle = ko.observable(true);
            this.sortProperty = ko.observable("Name");
            this.shouldBeActive = ko.computed(function () {
                return _this.currentUser().Role.Name !== UserRoles.Guest;
            });
            this.isAdmin = ko.computed(function () {
                return _this.currentUser().Role.Name === UserRoles.Admin;
            });
            this.fcQuery = ko.computed(function () {
                return "?sortProperty=" + _this.sortProperty() + "&isAsc=" + _this.isAscToggle()
                    + "&pageNumber=" + _this.currentPage() + "&elemPerPage=" + _this.pageElements()
                    + "&search=" + _this.searchFieldValue();
            });
            this.maxPageNumber = ko.computed(function () {
                return Math.ceil(App.Config.currentDataCount() / _this.pageElements());
            });
            this.fcQuery.subscribe(function () {
                var query = "?search=" + _this.searchFieldValue();
                App.Service.connManager.invokeGet(App.Service.endpoints.getFileCabinetCount, query)
                    .then(function (count) {
                    App.Config.currentDataCount(count);
                    _this.loadFileCabinets();
                });
            });
            this.searchFieldValue.subscribe(function () {
                _this.currentPage(1);
            });
        }
        FileCabinetsVm.prototype.loadFileCabinets = function () {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.getFileCabinets, this.fcQuery())
                .then(function (data) {
                var parsed = [];
                for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                    var fc = data_1[_i];
                    parsed.push(new DataVms.FileCabinetViewModel(fc));
                }
                _this.fileCabinets(parsed);
            });
        };
        FileCabinetsVm.prototype.addFileCabinet = function () {
            var _this = this;
            if (this.isFcModelValid()) {
                App.Service.connManager.invokePost(App.Service.endpoints.addFileCabinet, this.fcModalData().toModel())
                    .then(function (returnedFc) {
                    if (_this.fileCabinets().length == _this.pageElements()) {
                        _this.fileCabinets.pop();
                    }
                    _this.fileCabinets.unshift(new DataVms.FileCabinetViewModel(returnedFc));
                    _this.fcModalData(new DataVms.FileCabinetViewModel());
                    App.Utils.hideModal("#file-cabinet-modal");
                });
            }
            else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        };
        FileCabinetsVm.prototype.updateFileCabinet = function () {
            var _this = this;
            if (this.isFcModelValid()) {
                App.Service.connManager.invokePost(App.Service.endpoints.updateFileCabinet, this.fcModalData().toModel())
                    .then(function () {
                    for (var _i = 0, _a = _this.fileCabinets(); _i < _a.length; _i++) {
                        var el = _a[_i];
                        if (el.Guid == _this.fcModalData().Guid) {
                            _this.fileCabinets.replace(el, _this.fcModalData());
                            App.Utils.hideModal("#file-cabinet-modal");
                            break;
                        }
                    }
                    ;
                });
            }
            else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        };
        FileCabinetsVm.prototype.getNextPage = function () {
            if (this.currentPage() < this.maxPageNumber()) {
                this.currentPage(this.currentPage() + 1);
            }
        };
        FileCabinetsVm.prototype.getPrevPage = function () {
            if (this.currentPage() > 1) {
                this.currentPage(this.currentPage() - 1);
            }
        };
        FileCabinetsVm.prototype.isFcModelValid = function () {
            return App.Utils.isInputValid(this.fcModalData().Name());
        };
        FileCabinetsVm.prototype.sortByProperty = function (propName) {
            if (this.sortProperty() != propName) {
                this.sortProperty(propName);
                this.isAscToggle(true);
            }
            else {
                this.isAscToggle(!this.isAscToggle());
            }
        };
        FileCabinetsVm.prototype.setModalData = function (fc) {
            if (fc) {
                var clone = new DataVms.FileCabinetViewModel(fc.toModel());
                this.fcModalData(clone);
            }
            else {
                this.fcModalData(new DataVms.FileCabinetViewModel());
            }
        };
        FileCabinetsVm.prototype.openFcModal = function (fc) {
            if (fc) {
                this.isEdit(true);
            }
            else {
                this.isEdit(false);
            }
            this.setModalData(fc);
        };
        return FileCabinetsVm;
    }());
    Vms.FileCabinetsVm = FileCabinetsVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=FileCabinetsVm.js.map