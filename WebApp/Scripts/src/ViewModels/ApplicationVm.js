var Vms;
(function (Vms) {
    var ApplicationVm = /** @class */ (function () {
        function ApplicationVm() {
            this.currentUser = ko.observable(new DataVms.UserViewModel(App.Service.currentUser));
            this.fileCabinetsVm = ko.observable(new Vms.FileCabinetsVm());
            this.documentsVm = ko.observable(null);
            this.isFcViewVisible = ko.observable(true);
            this.modalTitle = ko.observable("");
            this.modalText = ko.observable("");
            this.modalCb = function () { };
            this.openFcList();
        }
        ApplicationVm.prototype.openFileCabinet = function (fileCabinet) {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.getDocumentCount, fileCabinet.Guid)
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.activateDocView();
                _this.documentsVm().setFileCabinet(fileCabinet.toModel());
            });
        };
        ApplicationVm.prototype.openArchive = function () {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.getArchivedCount)
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.activateDocView();
                _this.documentsVm().setArchive();
            });
        };
        ApplicationVm.prototype.openTrash = function () {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.getDeletedCount)
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.activateDocView();
                _this.documentsVm().setTrash();
            });
        };
        ApplicationVm.prototype.openFcList = function () {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.getFileCabinetCount)
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.activateFcView();
                _this.fileCabinetsVm().loadFileCabinets();
            });
        };
        ApplicationVm.prototype.deleteFileCabinet = function (caller) {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.deleteFileCabinet, caller.Guid)
                .then(function () {
                _this.fileCabinetsVm().fileCabinets.remove(caller);
            });
        };
        ApplicationVm.prototype.deleteFileCabinetFromDocView = function (caller) {
            var _this = this;
            App.Service.connManager.invokeGet(App.Service.endpoints.deleteFileCabinet, caller.Guid)
                .then(function () {
                _this.openFcList();
                _this.fileCabinetsVm().fileCabinets.remove(caller);
            });
        };
        ApplicationVm.prototype.openConfirmModal = function (titleKey, textKey, cb, name) {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            }
            else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        };
        ApplicationVm.prototype.activateFcView = function () {
            this.documentsVm(null);
            this.fileCabinetsVm(new Vms.FileCabinetsVm());
        };
        ApplicationVm.prototype.activateDocView = function () {
            this.fileCabinetsVm(null);
            this.documentsVm(new Vms.DocumentsVm());
        };
        return ApplicationVm;
    }());
    Vms.ApplicationVm = ApplicationVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=ApplicationVm.js.map