﻿namespace Vms {
    export class DocumentsVm {
        private currentUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable(new DataVms.UserViewModel(App.Service.currentUser));

        //General
        private documents: KnockoutObservableArray<DataVms.DocumentViewModel>
            = ko.observableArray(new Array<DataVms.DocumentViewModel>());

        private currentSelection: string[] = [];

        private currentFileCabinet: Models.FileCabinet = new Models.FileCabinet();
        private sectionTitle: KnockoutObservable<string> = ko.observable("");

        //Modal data
        private docModalData: KnockoutObservable<DataVms.DocumentViewModel> = ko.observable(new DataVms.DocumentViewModel());
        private addDocIsCopy: KnockoutObservable<boolean> = ko.observable(false);
        private addDocSelectValue: KnockoutObservable<string> = ko.observable("");
        private modalDocTypeName: KnockoutObservable<string>;
        private archiveName: KnockoutObservable<string> = ko.observable("");
        private exportData: KnockoutObservableArray<string> = ko.observableArray([]);

        //Authorization
        private shouldBeActive: KnockoutObservable<boolean>;
        private isArchive: KnockoutObservable<boolean> = ko.observable(false);
        private isTrash: KnockoutObservable<boolean> = ko.observable(false);
        private isAdmin: KnockoutObservable<boolean>;
        private isGuest: KnockoutObservable<boolean>;
        private isAllChecked: KnockoutComputed<boolean>;

        //File view settings
        private currentFileExt: KnockoutObservable<string> = ko.observable("");
        private shouldHavePreview: KnockoutObservable<boolean> = ko.observable(false);
        private isDoctypeActive: KnockoutObservable<boolean> = ko.observable(true);

        //Pagination
        private currentPage: KnockoutObservable<number> = ko.observable(1);
        public pageElements: KnockoutObservable<number> = ko.observable(5);
        public maxPageNumber: KnockoutComputed<number>;

        //Search
        private searchType: KnockoutObservable<string> = ko.observable("");
        private searchFieldValue: KnockoutObservable<string> = ko.observable("");

        //Sort
        private isAscToggle: KnockoutObservable<boolean> = ko.observable(true);
        private sortProperty: KnockoutObservable<string> = ko.observable('Name');
        private documentsQuery: KnockoutComputed<string>;

        constructor() {
            this.modalDocTypeName = ko.computed(() => {
                return App.Service.dataMapper.getDocumentTypeName(this.addDocSelectValue());
            });
            this.isAllChecked = ko.computed(() => {
                for (let doc of this.documents()) {
                    if (!doc.isChecked()) {
                        return false;
                    }
                }
                return true;
            })
            this.isAdmin = ko.computed(() => {
                return this.currentUser().Role.Name === UserRoles.Admin;
            });
            this.isGuest = ko.computed(() => {
                return this.currentUser().Role.Name === UserRoles.Guest;
            });
            this.maxPageNumber = ko.computed(() => {
                return Math.ceil(App.Config.currentDataCount() / this.pageElements());
            });
            this.shouldBeActive = ko.computed(() => {
                if (this.isArchive() || this.isTrash()) {
                    return false;
                } else {
                    return !this.isGuest();
                }
            });
            this.documentsQuery = ko.computed(() => {
                let query = "?sortProperty=" + this.sortProperty() + "&isAsc=" + this.isAscToggle()
                    + "&pageNumber=" + this.currentPage() + "&elemPerPage=" + this.pageElements()
                    + "&search=" + this.searchFieldValue();

                if (this.searchType()) {
                    query += "&searchType=" + this.searchType();
                }
                return query;
            });

            this.documentsQuery.subscribe(() => {
                this.getDocumentCount()
                    .then((count: number) => {
                        App.Config.currentDataCount(count);
                        this.loadDocs();
                    });
            });

            this.searchType.subscribe(() => {
                this.currentPage(1);
            });
            this.searchFieldValue.subscribe(() => {
                this.currentPage(1);
            });
            this.pageElements.subscribe(() => {
                if (this.currentPage() > this.maxPageNumber()) {
                    this.currentPage(this.maxPageNumber());
                }
            });
        }

        public setFileCabinet(fileCabinet: Models.FileCabinet): void {
            this.currentFileCabinet = fileCabinet;
            this.sectionTitle(this.currentFileCabinet.Name);
            this.isArchive(false);
            this.isTrash(false);
            this.loadDocs();
        }

        public setArchive(): void {
            this.sectionTitle(App.Service.locResources['ArchiveTitle']);
            this.isArchive(true);
            this.isTrash(false);
            this.loadDocs();
        }

        public setTrash(): void {
            this.sectionTitle(App.Service.locResources['TrashTitle']);
            this.isArchive(false);
            this.isTrash(true);
            this.loadDocs();
        }

        private addDocument(doc: Models.Document): void {
            App.Service.connManager.invokePost<Models.Document>(App.Service.endpoints.addDocument, doc)
                .then((received: Models.Document) => {
                    if (this.documents().length == this.pageElements()) {
                        this.documents.pop();
                    }
                    this.documents.unshift(new DataVms.DocumentViewModel(received));

                    App.Utils.hideProgressBar();
                    App.Utils.hideModal("#add-document-modal");
                })
                .catch(() => {
                    App.Utils.hideProgressBar();
                    App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                });
        }

        private deleteDocuments(guids: string[]): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.deleteDocuments, "?guids=" + guids.join())
                .then(() => {
                    this.documents(this.documents().filter((doc) => {
                        return guids.indexOf(doc.Guid) === -1;
                    }));
                });
        }

        private archivateDocument(doc: DataVms.DocumentViewModel): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.archivateDocument, doc.Guid)
                .then(() => {
                    this.documents.remove(doc);
                });
        }

        private restoreDeletedDocuments(guids: string[]): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.restoreDeletedDocuments, "?guids=" + guids.join())
                .then(() => {
                    this.documents(this.documents().filter((doc) => {
                        return guids.indexOf(doc.Guid) === -1;
                    }));
                });
        }

        private restoreAllDeletedDocuments(): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.restoreDeletedDocuments, "?all=true")
                .then(() => {
                    this.documents([]);
                });
        }

        private restoreArchivedDocuments(guids: string[]): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.restoreArchivedDocuments, "?guids=" + guids.join())
                .then(() => {
                    this.documents(this.documents().filter((doc) => {
                        return guids.indexOf(doc.Guid) === -1;
                    }));
                });
        }

        private restoreAllArchivedDocuments(): void {
            App.Service.connManager.invokeGet<void>(App.Service.endpoints.restoreArchivedDocuments, "?all=true")
                .then(() => {
                    this.documents([]);
                });
        }

        private updateDocument(): void {
            if (this.isDocModelValid()) {
                App.Service.connManager.invokePost<Models.Document>(App.Service.endpoints.updateDocument, this.docModalData().getModel())
                    .then(() => {
                        console.log(this.docModalData());
                        for (let single of this.documents()) {
                            if (single.Guid == this.docModalData().Guid) {
                                this.documents.replace(single, this.docModalData());
                                App.Utils.hideModal("#view-document-modal");
                                break;
                            }
                        }
                    });
            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private copyDocument(): void {
            if (this.isDocModelValid()) {
                App.Service.connManager.invokePost<Models.Document>(App.Service.endpoints.copyDocument, this.docModalData().getModel())
                    .then((newDocument: Models.Document) => {
                        if (this.documents().length == this.pageElements()) {
                            this.documents.pop();
                        }
                        this.documents.unshift(new DataVms.DocumentViewModel(newDocument));
                        App.Utils.hideModal("#add-document-modal");
                    });
            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private archivateCurrentFileCabinet(): void {
            App.Service.connManager.invokeGet<Models.FileCabinet>(App.Service.endpoints.archivateFileCabinet, this.currentFileCabinet.Guid)
                .then(() => {
                    this.documents([]);
                });
        }

        private setArchiveName(name: string): void {
            let date: Date = new Date;
            let displayedName: string = name.replace(/\s/g, '') + "." + date.toISOString().substring(0, 10);
            this.archiveName(displayedName);
        }
        
        private setExportModalData(guids: string[]): void {
            this.shouldHavePreview(false);
            this.exportData(guids);

            if (guids.length === 1) {
                let docs = this.documents().filter((el) => {
                    return el.Guid == guids[0];
                });
                this.setArchiveName(docs[0].Name());
                this.setDocModalData(docs[0]);
            } else {
                this.setArchiveName(this.currentFileCabinet.Name);
            }
            App.Utils.showModal("#export-document-modal");
        }

        private openFileView(doc: DataVms.DocumentViewModel): void {
            let ext = App.Utils.getFileExtension(doc.FilePath());

            this.addDocIsCopy(false);
            this.isDoctypeActive(false);
            this.shouldHavePreview(App.Config.supportedPreviewExtensions.indexOf(ext) !== -1);
            this.currentFileExt(ext);
            this.setDocModalData(doc);
        }

        private setDocModalData(doc?: DataVms.DocumentViewModel): void {
            let clone: DataVms.DocumentViewModel = new DataVms.DocumentViewModel(doc.getModel());
            if (this.addDocIsCopy()) {
                clone.Name(clone.Name() + " - copy");
            }
            this.docModalData(clone);
            this.addDocSelectValue(clone.Type().Guid);
        }

        private openSaveNewDocumentForm(isCopy: boolean, doc: DataVms.DocumentViewModel): void {
            this.addDocIsCopy(isCopy);
            this.isDoctypeActive(!isCopy);
            this.shouldHavePreview(false);

            if (isCopy) {
                this.setDocModalData(doc);
            } else {
                this.setDocModalData(new DataVms.DocumentViewModel());
            }
        }

        private modifySelection(doc: DataVms.DocumentViewModel): boolean {
            if (doc.isChecked()) {
                let index = this.currentSelection.indexOf(doc.Guid);
                this.currentSelection.splice(index, 1);
            } else {
                this.currentSelection.push(doc.Guid);
            }

            doc.isChecked(!doc.isChecked());
            return true;
        }

        private importDocuments(): void {
            let file: File = $("#importFile")[0].files[0];
            let ext = App.Utils.getFileExtension(file.name);
            if (ext != ".zip") {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources["ZipFormatError"]);
                return;
            }

            App.Service.connManager.invokeGet<string>(App.Service.endpoints.getRandomGuid)
                .then((guid: string) => {
                    App.Service.connManager
                        .uploadFileByChunks(App.Service.endpoints.uploadChunks + guid, file)
                        .then((fileName: string) => {
                            let importInfo
                                = new Models.UploadedFileInfo(guid, this.currentFileCabinet.Guid, fileName);

                            App.Service.connManager
                                .invokePostDifferent<Models.UploadedFileInfo, Models.Document[]>(
                                    App.Service.endpoints.completeImport, importInfo
                                ).then((importedDocs) => {
                                    for (let doc of importedDocs) {
                                        this.documents.unshift(new DataVms.DocumentViewModel(doc));
                                    }

                                    App.Utils.hideProgressBar();
                                    App.Utils.hideModal("#import-documents-modal");
                                })
                                .catch(() => {
                                    App.Utils.hideProgressBar();
                                    App.Utils.alertInvalidDataError(".form-error-alerty", "Error!");
                                });

                        }).catch(() => {
                            App.Utils.hideProgressBar();
                            App.Utils.alertInvalidDataError(".form-error-alerty", "Error!");
                        });
                });
        }

        private exportDocuments(): void {
            if (App.Utils.isInputValid(this.archiveName())) {
                let url = App.Service.endpoints.exportDocuments + "?filename=" + this.archiveName() + "&guids=" + this.exportData().join();
                App.Utils.hideModal("#export-document-modal");
                window.location.href = url;

            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private exportCurrentFileCabinet(): void {
            if (App.Utils.isInputValid(this.archiveName())) {
                let url = App.Service.endpoints.exportFileCabinet + this.currentFileCabinet.Guid + "?filename=" + this.archiveName();
                App.Utils.hideModal("#export-document-modal");
                window.location.href = url;
            } else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        }

        private toggleAll(): boolean {
            if (this.isAllChecked()) {
                this.documents().map((el) => {
                    el.isChecked(false);
                    let index = this.currentSelection.indexOf(el.Guid);
                    this.currentSelection.splice(index, 1);

                });
            } else {
                this.documents().map((doc) => {
                    if (this.currentSelection.indexOf(doc.Guid) === -1) {
                        doc.isChecked(true);
                        this.currentSelection.push(doc.Guid);
                    }
                });
            }
            return true;
        }

        private loadDocs(): void {
            this.getDocs()
                .then((docs: Models.Document[]) => {
                    let parsed: DataVms.DocumentViewModel[] = [];
                    for (let doc of docs) {
                        let docVm = new DataVms.DocumentViewModel(doc);
                        if (this.currentSelection.indexOf(docVm.Guid) !== -1) {
                            docVm.isChecked(true);
                        }
                        parsed.push(docVm);
                    }
                    this.documents(parsed);
                });
        }

        private getDocs(): JQueryPromise<Models.Document[]> {
            if (this.isTrash()) {
                return App.Service.connManager.invokeGet<Models.Document[]>(App.Service.endpoints.getDeleted, this.documentsQuery());
            } else if (this.isArchive()) {
                return App.Service.connManager.invokeGet<Models.Document[]>(App.Service.endpoints.getArchived, this.documentsQuery());
            } else {
                let fullQuery: string = this.currentFileCabinet.Guid + this.documentsQuery();
                return App.Service.connManager.invokeGet<Models.Document[]>(App.Service.endpoints.getDocumentsInFc, fullQuery);
            }
        }

        private saveFile(): void {
            //let fd = new FormData();
            //fd.append("file", $("#File")[0].files[0]);
            //App.Service.connManager.invokeUpload<Models.UploadedFileInfo>(App.Service.endpoints.uploadFile, fd)
            //    .then((fileInfo: Models.UploadedFileInfo) => {
            //        let model: Models.Document = this.docModalData().getModel();
            //        model.TypeId = this.addDocSelectValue();
            //        model.FileCabinetId = this.currentFileCabinet.Guid;
            //        model.FilePath = fileInfo.FilePath;
            //        model.Guid = fileInfo.DocumentGuid;

            //        this.addDocument(model);
            //    });
            if (!this.isDocModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.connManager.invokeGet<string>(App.Service.endpoints.getRandomGuid)
                .then((guid: string) => {
                    App.Service.connManager
                        .uploadFileByChunks(App.Service.endpoints.uploadChunks + guid, $("#File")[0].files[0])
                        .then((fileName: string) => {
                            let fileInfo: Models.UploadedFileInfo =
                                new Models.UploadedFileInfo(guid, this.currentFileCabinet.Guid, fileName);

                            App.Service.connManager
                                .invokePostDifferent<Models.UploadedFileInfo, string>(App.Service.endpoints.completeUpload,
                                    fileInfo)
                                .then((filePath: string) => {
                                    let model: Models.Document = this.docModalData().getModel();
                                    model.TypeId = this.addDocSelectValue();
                                    model.FileCabinetId = this.currentFileCabinet.Guid;
                                    model.FilePath = filePath;
                                    model.Guid = guid;

                                    this.addDocument(model);
                                })
                                .catch(() => {
                                    App.Utils.hideProgressBar();
                                    App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                                });

                        }).catch(() => {
                            App.Utils.hideProgressBar();
                            App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                        });
                });
        }

        private getNextPage(): void {
            if (this.currentPage() < this.maxPageNumber()) {
                this.currentPage(this.currentPage() + 1);
            }
        }

        private getPrevPage(): void {
            if (this.currentPage() > 1) {
                this.currentPage(this.currentPage() - 1);
            }
        }

        private isDocModelValid(): boolean {
            return App.Utils.isInputValid(this.docModalData().Name()) && App.Utils.isInputValid(this.addDocSelectValue());
        }

        private sortByProperty(propName: string) {
            if (this.sortProperty() != propName) {
                this.sortProperty(propName);
                this.isAscToggle(true);
            } else {
                this.isAscToggle(!this.isAscToggle());
            }
        }

        private getDocumentCount(): JQueryPromise<number> {
            let query: string = "?search=" + this.searchFieldValue();
            if (this.searchType()) {
                query += "?searchType=" + this.searchType();
            }
            if (this.isTrash()) {
                return App.Service.connManager.invokeGet<number>(App.Service.endpoints.getDeletedCount, query);
            } else if (this.isArchive()) {
                return App.Service.connManager.invokeGet<number>(App.Service.endpoints.getArchivedCount, query);
            } else {
                return App.Service.connManager.invokeGet<number>(App.Service.endpoints.getDocumentCount, this.currentFileCabinet.Guid + query);
            }
        }
    }
}