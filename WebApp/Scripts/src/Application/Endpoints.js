var Api;
(function (Api) {
    var Endpoints = /** @class */ (function () {
        function Endpoints() {
            this._getFileCabinets = "/FileCabinets/";
            this._addFileCabinet = "/FileCabinets/Add/";
            this._deleteFileCabinet = "/FileCabinets/Delete/";
            this._archivateFileCabinet = "/FileCabinets/Archivate/";
            this._updateFileCabinet = "/FileCabinets/Update/";
            this._getFileCabinetCount = "/FileCabinets/Count/";
            this._exportFileCabinet = "/FileCabinets/Export/";
            this._getRoles = "/Roles/";
            this._getDocumentTypes = "/DocumentTypes/";
            this._getDocumentsInFc = "/Documents/";
            this._addDocument = "/Documents/Add/";
            this._deleteDocuments = "/Documents/Delete/";
            this._archivateDocument = "/Documents/Archivate/";
            this._exportDocuments = "/Documents/ExportDocuments";
            this._updateDocument = "/Documents/Update/";
            this._copyDocument = "/Documents/Copy/";
            this._getArchived = "/Documents/Archive/";
            this._getArchivedCount = "/Documents/Archive/Count/";
            this._getDeleted = "/Documents/Deleted/";
            this._getDeletedCount = "/Documents/Deleted/Count/";
            this._getFile = "/Documents/GetFile/";
            this._completeImport = "/Documents/CompleteImport/";
            this._restoreArchivedDocuments = "/Documents/RestoreArchive/";
            this._restoreDeletedDocuments = "/Documents/RestoreDeleted/";
            this._uploadFile = "/documents/uploadfile/";
            this._uploadChunks = "/documents/uploadchunks/";
            this._completeUpload = "/documents/completeupload/";
            this._getDocumentCount = "/documents/count/";
            this._getUsers = "/Users/GetUsers/";
            this._getRandomGuid = "/Home/GetRandomGuid/";
        }
        Object.defineProperty(Endpoints.prototype, "getFileCabinets", {
            get: function () {
                return this._getFileCabinets;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "addFileCabinet", {
            get: function () {
                return this._addFileCabinet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "deleteFileCabinet", {
            get: function () {
                return this._deleteFileCabinet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "archivateFileCabinet", {
            get: function () {
                return this._archivateFileCabinet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "updateFileCabinet", {
            get: function () {
                return this._updateFileCabinet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getFileCabinetCount", {
            get: function () {
                return this._getFileCabinetCount;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "exportFileCabinet", {
            get: function () {
                return this._exportFileCabinet;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getRoles", {
            get: function () {
                return this._getRoles;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getDocumentTypes", {
            get: function () {
                return this._getDocumentTypes;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getDocumentsInFc", {
            get: function () {
                return this._getDocumentsInFc;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getUsers", {
            get: function () {
                return this._getUsers;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "addDocument", {
            get: function () {
                return this._addDocument;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "deleteDocuments", {
            get: function () {
                return this._deleteDocuments;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "archivateDocument", {
            get: function () {
                return this._archivateDocument;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "exportDocuments", {
            get: function () {
                return this._exportDocuments;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "updateDocument", {
            get: function () {
                return this._updateDocument;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "copyDocument", {
            get: function () {
                return this._copyDocument;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getArchived", {
            get: function () {
                return this._getArchived;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getArchivedCount", {
            get: function () {
                return this._getArchivedCount;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getDeleted", {
            get: function () {
                return this._getDeleted;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getDeletedCount", {
            get: function () {
                return this._getDeletedCount;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "restoreArchivedDocuments", {
            get: function () {
                return this._restoreArchivedDocuments;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "restoreDeletedDocuments", {
            get: function () {
                return this._restoreDeletedDocuments;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "uploadFile", {
            get: function () {
                return this._uploadFile;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "uploadChunks", {
            get: function () {
                return this._uploadChunks;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "completeUpload", {
            get: function () {
                return this._completeUpload;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getFile", {
            get: function () {
                return this._getFile;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getDocumentCount", {
            get: function () {
                return this._getDocumentCount;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "getRandomGuid", {
            get: function () {
                return this._getRandomGuid;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Endpoints.prototype, "completeImport", {
            get: function () {
                return this._completeImport;
            },
            enumerable: true,
            configurable: true
        });
        return Endpoints;
    }());
    Api.Endpoints = Endpoints;
})(Api || (Api = {}));
//# sourceMappingURL=Endpoints.js.map