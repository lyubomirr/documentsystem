﻿namespace Api {
    export class Endpoints {
        private readonly _getFileCabinets: string = "/FileCabinets/";
        private readonly _addFileCabinet: string = "/FileCabinets/Add/";
        private readonly _deleteFileCabinet: string = "/FileCabinets/Delete/";
        private readonly _archivateFileCabinet: string = "/FileCabinets/Archivate/";
        private readonly _updateFileCabinet: string = "/FileCabinets/Update/";
        private readonly _getFileCabinetCount: string = "/FileCabinets/Count/";
        private readonly _exportFileCabinet: string = "/FileCabinets/Export/";

        private readonly _getRoles: string = "/Roles/";

        private readonly _getDocumentTypes: string = "/DocumentTypes/";

        private readonly _getDocumentsInFc: string = "/Documents/";
        private readonly _addDocument: string = "/Documents/Add/";
        private readonly _deleteDocuments: string = "/Documents/Delete/";
        private readonly _archivateDocument: string = "/Documents/Archivate/";
        private readonly _exportDocuments: string = "/Documents/ExportDocuments";
        private readonly _updateDocument: string = "/Documents/Update/";
        private readonly _copyDocument: string = "/Documents/Copy/";
        private readonly _getArchived: string = "/Documents/Archive/";
        private readonly _getArchivedCount: string = "/Documents/Archive/Count/";
        private readonly _getDeleted: string = "/Documents/Deleted/";
        private readonly _getDeletedCount: string = "/Documents/Deleted/Count/";
        private readonly _getFile: string = "/Documents/GetFile/";
        private readonly _completeImport: string = "/Documents/CompleteImport/";
        private readonly _restoreArchivedDocuments: string = "/Documents/RestoreArchive/";
        private readonly _restoreDeletedDocuments: string = "/Documents/RestoreDeleted/";
        private readonly _uploadFile: string = "/documents/uploadfile/";
        private readonly _uploadChunks: string = "/documents/uploadchunks/";
        private readonly _completeUpload: string = "/documents/completeupload/";
        private readonly _getDocumentCount: string = "/documents/count/";

        private readonly _getUsers: string = "/Users/GetUsers/";
        private readonly _getRandomGuid: string = "/Home/GetRandomGuid/";


        get getFileCabinets(): string {
            return this._getFileCabinets;
        }

        get addFileCabinet(): string {
            return this._addFileCabinet;
        }

        get deleteFileCabinet(): string {
            return this._deleteFileCabinet;
        }

        get archivateFileCabinet(): string {
            return this._archivateFileCabinet;
        }

        get updateFileCabinet(): string {
            return this._updateFileCabinet;
        }

        get getFileCabinetCount(): string {
            return this._getFileCabinetCount;
        }

        get exportFileCabinet(): string {
            return this._exportFileCabinet;
        }

        get getRoles(): string {
            return this._getRoles;
        }

        get getDocumentTypes(): string {
            return this._getDocumentTypes;
        }

        get getDocumentsInFc(): string {
            return this._getDocumentsInFc;
        }

        get getUsers(): string {
            return this._getUsers;
        }

        get addDocument(): string {
            return this._addDocument;
        }

        get deleteDocuments(): string {
            return this._deleteDocuments;
        }

        get archivateDocument(): string {
            return this._archivateDocument;
        }

        get exportDocuments(): string {
            return this._exportDocuments;
        }

        get updateDocument(): string {
            return this._updateDocument;
        }

        get copyDocument(): string {
            return this._copyDocument;
        }

        get getArchived(): string {
            return this._getArchived;
        }

        get getArchivedCount(): string {
            return this._getArchivedCount;
        }

        get getDeleted(): string {
            return this._getDeleted;
        }

        get getDeletedCount(): string {
            return this._getDeletedCount;
        }

        get restoreArchivedDocuments(): string {
            return this._restoreArchivedDocuments;
        }

        get restoreDeletedDocuments(): string {
            return this._restoreDeletedDocuments;
        }

        get uploadFile(): string {
            return this._uploadFile;
        }

        get uploadChunks(): string {
            return this._uploadChunks;
        }

        get completeUpload(): string {
            return this._completeUpload;
        }

        get getFile(): string {
            return this._getFile;
        }

        get getDocumentCount(): string {
            return this._getDocumentCount;
        }

        get getRandomGuid(): string {
            return this._getRandomGuid;
        }

        get completeImport(): string {
            return this._completeImport;
        }
    }
}