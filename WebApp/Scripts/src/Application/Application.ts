﻿namespace Global {
    export class Application {
        public Service: Services.ApplicationService;
        public Utils: Services.Utils;
        public Config: Services.Config;
    }
}