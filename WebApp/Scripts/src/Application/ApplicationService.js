var Services;
(function (Services) {
    var ApplicationService = /** @class */ (function () {
        function ApplicationService() {
        }
        ApplicationService.prototype.init = function () {
            var _this = this;
            var dfd = $.Deferred();
            this.connManager = new Api.ConnectionManager();
            this.endpoints = new Api.Endpoints();
            this.connManager.getResources()
                .then(function (locResources) {
                _this.locResources = locResources;
                _this.connManager.invokeGet(_this.endpoints.getRoles)
                    .then(function (roles) {
                    _this.connManager.invokeGet(_this.endpoints.getDocumentTypes)
                        .then(function (docTypes) {
                        for (var _i = 0, docTypes_1 = docTypes; _i < docTypes_1.length; _i++) {
                            var docType = docTypes_1[_i];
                            docType.Name = _this.locResources[docType.Name];
                        }
                        _this.dataMapper = new Api.DataMap(roles, docTypes);
                        _this.connManager.invokeGet(_this.endpoints.getUsers, _this.getCookie("UserGuid"))
                            .then(function (user) {
                            _this.currentUser = user;
                            dfd.resolve();
                        });
                    });
                });
            });
            return dfd.promise();
        };
        ApplicationService.prototype.getCookie = function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        };
        return ApplicationService;
    }());
    Services.ApplicationService = ApplicationService;
})(Services || (Services = {}));
//# sourceMappingURL=ApplicationService.js.map