﻿namespace Services {
    export class Config {
        public supportedPreviewExtensions: string[] = [".pdf", ".png", ".jpg", ".jpeg", ".bmp", ".txt", ".gif"];
        public currentDataCount: KnockoutObservable<number> = ko.observable(0);
        public pageElementsAvailableOptions: number[] = [1, 2, 5, 10, 20, 50, 100];
        public bytesPerUploadChunk: number = 4000000; //4000000
    }
}