﻿namespace Services {
    export class ApplicationService {
        public connManager: Api.ConnectionManager;
        public endpoints: Api.Endpoints;
        public dataMapper: Api.DataMap;
        public currentUser: Models.User;
        public locResources: Dictionary<string>;

        public init(): JQueryPromise<void> {

            let dfd = $.Deferred<void>();

            this.connManager = new Api.ConnectionManager();
            this.endpoints = new Api.Endpoints();

            this.connManager.getResources()
                .then((locResources: Dictionary<string>) => {
                    this.locResources = locResources;

                    this.connManager.invokeGet<Models.Role[]>(this.endpoints.getRoles)
                        .then((roles: Models.Role[]) => {
                            this.connManager.invokeGet<Models.DocumentType[]>(this.endpoints.getDocumentTypes)
                                .then((docTypes: Models.DocumentType[]) => {
                                    for (let docType of docTypes) {
                                        docType.Name = this.locResources[docType.Name];
                                    }

                                    this.dataMapper = new Api.DataMap(roles, docTypes);

                                    this.connManager.invokeGet<Models.User>(this.endpoints.getUsers, this.getCookie("UserGuid"))
                                        .then((user: Models.User) => {
                                            this.currentUser = user;
                                            dfd.resolve();
                                        })
                                });
                        });
                });         

            return dfd.promise();
        }

        private getCookie(cname: string): string {
            let name = cname + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    }
}
