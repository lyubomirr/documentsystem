﻿type CallbackFunction = (...args: any[]) => any;
type Dictionary<T> = { [key: string]: T };