﻿namespace Models {
    export class Document {
        constructor(
            public Guid: string = "",
            public Name: string = "",
            public Description: string = "",
            public TypeId: string = "",
            public StoreDate: string = "",
            public Amount: number = 0,
            public Vat: number = 0,
            public TotalAmount: number = 0,
            public LastModifiedUser: string = "",
            public LastModifiedDate: string = "",
            public Archived: boolean = false,
            public Deleted: boolean = false,
            public FileCabinetId: string = "",
            public FilePath: string = "",
            public CopyFromDocumentId: string = "",
            public Subject: string = "",
            public OwnerId: string = "",
            public Owner: User = new Models.User(),
            public RegDocNumber: string = "",
            public Contact: string = "",
            public Company: string = ""
        ) { }
    }
}