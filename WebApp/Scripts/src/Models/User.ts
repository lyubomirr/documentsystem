﻿namespace Models {
    export class User {
        constructor(
            public Guid: string = "",
            public Username: string = "",
            public Password: string = "",
            public Salt: string = "",
            public RoleId: string = ""
        ) { }    
    }
}