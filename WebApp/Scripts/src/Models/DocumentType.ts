﻿namespace Models {
    export class DocumentType {
        constructor(
            public Guid: string = "",
            public Name: string = ""
        ) { }
    }
}