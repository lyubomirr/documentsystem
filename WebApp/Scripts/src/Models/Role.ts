﻿namespace Models {
    export class Role {
        constructor(
            public Guid: string = "",
            public Name: string = ""
        ) {}
    }
}