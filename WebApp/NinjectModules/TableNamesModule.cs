﻿using Configuration;
using Interfaces.Services;
using Ninject.Modules;

namespace WebApp.NinjectModules
{
    public class TableNamesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<string>().ToConstant("Documents").Named("DocumentsTableName");
            Bind<string>().ToConstant("FileCabinets").Named("FileCabinetsTableName");
            Bind<string>().ToConstant("Users").Named("UsersTableName");
            Bind<string>().ToConstant("Roles").Named("RolesTableName");
            Bind<string>().ToConstant("DocumentTypes").Named("DocumentTypesTableName");
        }
    }
}