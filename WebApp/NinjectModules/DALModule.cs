﻿using DataAccessLayer;
using DatabaseConnectionLayer;
using Interfaces.Data;
using Ninject.Modules;
using System.Configuration;

namespace WebApp.NinjectModules
{
    public class DalModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDataAdapter>().To<DatabaseDataAdapter>()
               .WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            Bind<IDataFacade>().To<DatabaseDataFacade>();
            Bind<IDataQuery>().To<DataQuery>();
        }
    }
}