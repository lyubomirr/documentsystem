﻿using Interfaces.Services;
using Ninject.Modules;
using DbServices;

namespace WebApp.NinjectModules
{
    public class ServicesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocumentsService>().To<DocumentsService>();
            Bind<IDocumentTypesService>().To<DocumentTypesService>();
            Bind<IFileCabinetsService>().To<FileCabinetsService>();
            Bind<IRolesService>().To<RolesService>();
            Bind<IUsersService>().To<UsersService>();
        }
    }
}