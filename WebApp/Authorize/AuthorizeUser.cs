﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeUser : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return (
               httpContext.Request.Cookies.Get("AuthToken") != null && httpContext.Session["AuthToken"] != null
               && httpContext.Request.Cookies.Get("AuthToken").Value.ToString() == httpContext.Session["AuthToken"].ToString()
               );
        }
    }
}