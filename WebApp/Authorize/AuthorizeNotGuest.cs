﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Authorize
{
        [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
        public class AuthorizeNotGuest : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                return httpContext.Session["Role"].ToString() == "Admin" || httpContext.Session["Role"].ToString() == "Regular";
            }
        }
}