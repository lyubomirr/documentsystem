﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Configuration
{
    public static class TableMapper
    {
        public static TableMapping GetTableMapping(string tableName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TableMappingConfig));
            TableMappingConfig tableMapping;
            using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "\\TableMappingConfig.xml", FileMode.Open, FileAccess.Read))
            {
                tableMapping = (TableMappingConfig)serializer.Deserialize(fs);
            }
            return tableMapping.TableMappings.FirstOrDefault(i => i.TableName == tableName);
        }
    }
}
