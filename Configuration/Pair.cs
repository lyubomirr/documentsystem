﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Serialization;

namespace Configuration
{
    [Serializable]
    public class Pair 
    {
        [XmlAttribute]
        public string Model { get; set; }

        [XmlAttribute]
        public string DataSource { get; set; }
    }
}
