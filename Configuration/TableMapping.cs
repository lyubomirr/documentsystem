﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Serialization;

namespace Configuration
{   
    [Serializable]
    public class TableMapping
    {
        [XmlElement("Pair")]
        public List<Pair> Pairs { get; set; }
        
        [XmlAttribute]
        public string TableName { get; set; }
    }
}
