﻿using System;
using System.Collections.Generic;
using Configuration;
using System.Globalization;
using Interfaces.Data;

namespace DataAccessLayer
{
    public class DatabaseDataFacade : IDataFacade
    {
        private IDataAdapter _dataAdapter;

        public DatabaseDataFacade(IDataAdapter dataAdapter)
        {
            _dataAdapter = dataAdapter;
        }

        private bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        private Dictionary<string, object> constructObjectRawData<ModelT>(Type type, List<Pair> tableColumns, ModelT originalObject) where ModelT : class
        {
            Dictionary<string, object> parsedObj = new Dictionary<string, object>();
            foreach (var mapping in tableColumns)
            {
                var propType = type.GetProperty(mapping.Model).PropertyType;

                if (propType == typeof(DateTime) || propType == typeof(DateTime?))
                {
                    var propValue = type.GetProperty(mapping.Model).GetValue(originalObject);

                    if(propValue != null)
                    {
                        DateTime date = (DateTime)propValue;
                        parsedObj.Add(mapping.DataSource, "'" + date.ToString("yyyy-MM-dd") + "'");
                    }
                }
                else if (propType == typeof(string))
                {
                    var value = type.GetProperty(mapping.Model).GetValue(originalObject);
                    if(value != null)
                    {
                        string newVal = value.ToString();
                        newVal = newVal.Replace("'", "''");
                        parsedObj.Add(mapping.DataSource, "'" + newVal + "'");
                    }


                }
                else if(propType == typeof(Guid?) || propType == typeof(Guid))
                {
                    var propValue = type.GetProperty(mapping.Model).GetValue(originalObject);

                    if (propValue != null)
                    {
                        parsedObj.Add(mapping.DataSource, "'" + propValue.ToString() + "'");
                    }
                }
                else if (propType == typeof(bool))
                {
                    parsedObj.Add(mapping.DataSource, Convert.ToInt16(type.GetProperty(mapping.Model).GetValue(originalObject)));
                }
                else if(propType == typeof(decimal?) || propType == typeof(decimal))
                {
                    var propValue = type.GetProperty(mapping.Model).GetValue(originalObject);

                    if (propValue != null)
                    {
                        decimal value = (decimal)propValue;
                        parsedObj.Add(mapping.DataSource, value.ToString(CultureInfo.InvariantCulture));
                    }
                }
                else
                {
                    parsedObj.Add(mapping.DataSource, type.GetProperty(mapping.Model).GetValue(originalObject));
                }
            }

            return parsedObj;
        }

        public bool AddObject<ModelT>(ModelT newObject, string tableName) where ModelT : class
        {
            Type type = newObject.GetType();
            List<Pair> tableColumns = TableMapper.GetTableMapping(tableName).Pairs;

            Dictionary<string, object> parsedObj = constructObjectRawData(type, tableColumns, newObject);

            if (_dataAdapter.AddData(tableName, new List<Dictionary<string, object>>() { parsedObj }))
            {
                return true;
            }

            return false;
        }
        public bool DeleteObject(IDataQuery query, string tableName)
        {
            return _dataAdapter.DeleteData(tableName, query);
        }
        public List<ModelT> GetTableData<ModelT>(IDataQuery query, string tableName) where ModelT : class, new()
        {
            List<Pair> tableColumns = TableMapper.GetTableMapping(tableName).Pairs;
            List<ModelT> result = new List<ModelT>();
            List<Dictionary<string, object>> rawData = _dataAdapter.GetData(tableName, query);

            foreach (Dictionary<string, object> row in rawData)
            {
                ModelT single = new ModelT();
                Type type = single.GetType();

                foreach (var mapping in tableColumns)
                {
                    if (row.TryGetValue(mapping.DataSource, out object rowValue))
                    {
                        var propertyType = type.GetProperty(mapping.Model).PropertyType;
                        if(rowValue != DBNull.Value)
                        {
                            type.GetProperty(mapping.Model).SetValue(single, rowValue);
                        }                         
                    }
                }

                result.Add(single);
            }

            return result;
        }

        public List<ModelT> GetJoinedTableData<ModelT, JoinT>(IDataQuery query, string tableName, string populatedProperty)
            where ModelT : class, new()
            where JoinT : class, new()
        {
            List<Pair> tableColumns = TableMapper.GetTableMapping(tableName).Pairs;
            List<Pair> joinTableColumns = TableMapper.GetTableMapping(query.GetJoinedTable()).Pairs;
            List<ModelT> result = new List<ModelT>();

            List<Dictionary<string, object>> rawData = _dataAdapter.GetData(tableName, query);

            foreach (Dictionary<string, object> row in rawData)
            {
                ModelT single = new ModelT();
                JoinT toJoin = new JoinT();
                Type mainType = single.GetType();
                Type joinedType = toJoin.GetType();

                foreach (var mapping in tableColumns)
                {
                    if (row.TryGetValue(mapping.DataSource, out object rowValue))
                    {
                        var propertyType = mainType.GetProperty(mapping.Model).PropertyType;
                        if (rowValue != DBNull.Value)
                        {
                            mainType.GetProperty(mapping.Model).SetValue(single, rowValue);
                        }
                    }
                }

                foreach (var mapping in joinTableColumns)
                {
                    if (row.TryGetValue(mapping.DataSource, out object rowValue))
                    {
                        var propertyType = joinedType.GetProperty(mapping.Model).PropertyType;
                        if (rowValue != DBNull.Value)
                        {
                            joinedType.GetProperty(mapping.Model).SetValue(toJoin, rowValue);
                        }
                    }
                }

                mainType.GetProperty(populatedProperty).SetValue(single, toJoin);
                result.Add(single);
            }

            return result;
        }

        public bool ModifyObject<ModelT>(ModelT modified, IDataQuery query, string tableName) where ModelT : class
        {
            Type type = modified.GetType();
            List<Pair> tableColumns = TableMapper.GetTableMapping(tableName).Pairs;

            Dictionary<string, object> parsedObj = constructObjectRawData(type, tableColumns, modified);
            return _dataAdapter.UpdateData(tableName, parsedObj, query);
        }
        public int GetTableRowCount(string tableName, IDataQuery dq = null)
        {
            return _dataAdapter.GetTableRowCount(tableName, dq);
        }

    }
}
