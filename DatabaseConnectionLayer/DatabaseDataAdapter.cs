﻿using Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DatabaseConnectionLayer
{
    public class DatabaseDataAdapter : Interfaces.Data.IDataAdapter
    {
        private string _connectionString;
        public DatabaseDataAdapter()
        {

        }
        public DatabaseDataAdapter(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool AddData(string tableName, List<Dictionary<string, object>> data)
        {
            int rowsAffected = 0;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                foreach (Dictionary<string, object> row in data)
                {
                    int count = row.Count;

                    List<string> keys = new List<string>();
                    List<string> values = new List<string>();

                    foreach(var field in row)
                    {
                        if(field.Value != null)
                        {
                            keys.Add(field.Key);
                            values.Add(field.Value.ToString());
                        }
                    }

                    string keysJoined = "(" + String.Join(",", keys) + ")";
                    string valuesJoined = "(" + String.Join(",", values) + ")";

                    string cmdText = $"INSERT INTO {tableName} {keysJoined} VALUES {valuesJoined};";

                    using (SqlCommand command = new SqlCommand(cmdText, conn))
                    {
                        rowsAffected += command.ExecuteNonQuery();
                    }
                }
            }

            return rowsAffected > 0;
        }

        public bool DeleteData(string tableName, IDataQuery query)
        {
            string cmdTxt = $"DELETE FROM {tableName}" + query.ToSql();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand(cmdTxt, conn))
                {
                    return command.ExecuteNonQuery() > 0;
                }
            }
        }

        public List<Dictionary<string, object>> GetData(string tableName, IDataQuery query)
        {
            DataTable table = new DataTable(tableName);
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            string cmdText = $"SELECT * FROM {tableName}";
            if (query != null)
            {
                cmdText += query.ToSql();
            }

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(table);
                }
            }

            foreach (DataRow row in table.Rows)
            {
                Dictionary<string, object> currentRow = new Dictionary<string, object>();

                foreach (DataColumn column in table.Columns)
                {
                    currentRow.Add(column.ToString(), row[column]);
                }

                data.Add(currentRow);
            }

            return data;
        }

        public void SetSource(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool UpdateData(string tableName, Dictionary<string, object> newData, IDataQuery query)
        {
            int rowsAffected = 0;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                string cmdTxt = $"UPDATE {tableName} SET ";
                string vals = "";

                foreach (KeyValuePair<string, object> item in newData)
                {
                    if(item.Value != null)
                    {
                        vals += (item.Key + "=" + item.Value + ", ");
                    }
                }

                vals = vals.Remove(vals.Length - 2);
                cmdTxt += vals;
                cmdTxt += query.ToSql();

                using (SqlCommand command = new SqlCommand(cmdTxt, conn))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
            }

            return rowsAffected > 0;
        }

        public int GetTableRowCount(string tableName, IDataQuery query)
        {

            DataTable data = new DataTable(tableName);
            string cmdText = $"SELECT COUNT(*) FROM {tableName}";

            if (query != null)
            {
                cmdText += query.ToSql();
            }

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(cmdText, conn))
                {
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(data);
                }
            }

            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn column in data.Columns)
                {
                    return (int)row[column];
                }
            }

            return 0;
        }
    }
}
