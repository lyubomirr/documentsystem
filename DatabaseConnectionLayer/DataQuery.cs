﻿using Configuration;
using Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseConnectionLayer
{
    public class DataQuery : IDataQuery
    {
        private string _sortField;
        private string _joinTable;
        private string _joinForeignKey;
        private string _joinPrimaryKey;
        private bool _isAsc;
        private int? _pageNumber;
        private int _elemPerPage;
        private Dictionary<string, object> _queries;
        private Dictionary<string, object> _searches;
        private TableMapping _tableMapping;

        public DataQuery()
        {
            _queries = new Dictionary<string, object>();
            _searches = new Dictionary<string, object>();
        }

        public IDataQuery Init(string tableName)
        {
            _tableMapping = TableMapper.GetTableMapping(tableName);

            if (_tableMapping == null)
            {
                throw new ArgumentException("Invalid table name!");
            }

            _queries = new Dictionary<string, object>();
            _searches = new Dictionary<string, object>();
            _sortField = null;
            _isAsc = true;
            _pageNumber = null;

            return this;
        }

        public IDataQuery AddQuery(string property, string value)
        {
            if(!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(value))
            {
                string dbField = GetField(property);
                _queries.Add(dbField, $"'{value}'");
            }
            return this;
        }

        public IDataQuery AddQuery(string property, object value)
        {
            if (!String.IsNullOrEmpty(property) && value != null)
            {
                string dbField = GetField(property);
                _queries.Add(dbField, value);
            }
            return this;
        }

        public IDataQuery SortBy(string sortProperty, bool? isAsc = true)
        {
            if(!String.IsNullOrEmpty(sortProperty))
            {
                string dbSortField = GetField(sortProperty);
                _sortField = dbSortField;
                _isAsc = isAsc ?? true;
            }
            return this;
        }

        public IDataQuery GetPage(int? pageNumber, int? elemPerPage = null)
        {
            if(pageNumber != null)
            {
                _pageNumber = pageNumber;
                _elemPerPage = elemPerPage ?? 5;
            }
            return this;
        }

        public IDataQuery SearchBy(string property, object value)
        {
            if (!String.IsNullOrEmpty(property) && value != null)
            {
                string dbField = GetField(property);
                _searches.Add(dbField, value);
            }
            return this;
        }

        public IDataQuery SearchBy(string[] properties, object value)
        {
            foreach(string property in properties)
            {
                if(!String.IsNullOrEmpty(property) && value != null)
                {
                    string dbField = GetField(property);
                    _searches.Add(dbField, value);
                }
            }
            return this;
        }

        public IDataQuery Join(string joinTable, string foreignProperty, string primaryProperty)
        {
            var joinTableMapping = TableMapper.GetTableMapping(joinTable);
            if (joinTableMapping == null)
            {
                throw new ArgumentException($"No such table in DB as {joinTable}");
            }

            var primaryPropertyPair = joinTableMapping.Pairs.FirstOrDefault(pair => pair.Model == primaryProperty);
            if (primaryPropertyPair == null)
            {
                throw new ArgumentException($"No field corresponds to property {primaryProperty} in table {joinTable}!");
            }

            _joinTable = joinTable;
            _joinPrimaryKey = primaryPropertyPair.DataSource;
            _joinForeignKey = GetField(foreignProperty);

            return this;
        }

        public string GetJoinedTable()
        {
            return _joinTable;
        }
        public string ToSql()
        {
            string sql = "";

            if (!String.IsNullOrEmpty(_joinTable)
                && !String.IsNullOrEmpty(_joinForeignKey)
                && !String.IsNullOrEmpty(_joinPrimaryKey)
                )
            {
                sql += $" LEFT JOIN {_joinTable} ON {_tableMapping.TableName}.{_joinForeignKey} = {_joinTable}.{_joinPrimaryKey}";
            }
            if (_queries.Count > 0)
            {
                sql += " WHERE ";
                foreach (var query in _queries)
                {
                    sql += ($"{query.Key}={query.Value} AND ");
                }

                if(_searches.Count == 0)
                {
                    //We remove the last AND statement.
                    sql = sql.Substring(0, sql.Length - 5);
                }
            }
            
            if(_searches.Count > 0)
            {
                if(_queries.Count == 0)
                {
                    sql += " WHERE ";
                }
                sql += "(";
                foreach(var search in _searches)
                {
                    sql += ($"{search.Key} LIKE '%{search.Value}%' OR ");
                }

                //Remove last OR .
                sql = sql.Substring(0, sql.Length - 4);
                sql += ")";
            }

            if(!String.IsNullOrEmpty(_sortField))
            {
                sql += $" ORDER BY {_sortField}";
                sql = _isAsc ? sql + " ASC" : sql + " DESC";

                if (_pageNumber != null)
                {
                    sql += $" OFFSET(({_pageNumber} - 1) * {_elemPerPage}) ROWS FETCH NEXT {_elemPerPage} ROWS ONLY";
                }

            }
            
            return sql;
        }

        private string GetField(string propertyName)
        {
            Pair propertyPair = _tableMapping.Pairs.FirstOrDefault(pair => pair.Model == propertyName);
            if(propertyPair == null)
            {
                throw new ArgumentException($"No field corresponds to property {propertyName} in table {_tableMapping.TableName}!");
            }
            return propertyPair.DataSource;
        }

       
    }
}
