﻿using Interfaces.Data;

namespace DbServices
{
    public abstract class DbService
    {
        protected IDataFacade _dataFacade;
        protected IDataQuery _dq;
        protected string _tableName;

        protected DbService(IDataFacade dataFacade, IDataQuery dq, string tableName)
        {
            _dataFacade = dataFacade;
            _dq = dq;
            _tableName = tableName;
        }

    }
}