﻿using Interfaces.Data;
using Interfaces.Services;
using Models;
using Ninject;
using System.Collections.Generic;

namespace DbServices
{
    public class DocumentTypesService : DbService, IDocumentTypesService
    {
        public DocumentTypesService(IDataFacade dataFacade, IDataQuery dq, 
            [Named("DocumentTypesTableName")] string tableName)
            : base(dataFacade, dq, tableName)
        { }

        public List<DocumentType> GetDocumentTypes(IDataQuery query = null)
        {
            return _dataFacade.GetTableData<DocumentType>(query, _tableName);
        }
    }
}