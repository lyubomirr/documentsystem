﻿using Interfaces.Data;
using Interfaces.Services;
using Models;
using Ninject;
using System.Collections.Generic;

namespace DbServices
{
    public class FileCabinetsService : DbService, IFileCabinetsService
    {
        public FileCabinetsService(IDataFacade dataFacade, IDataQuery dq, 
            [Named("FileCabinetsTableName")] string tableName)
            : base(dataFacade, dq, tableName)  { }

        public List<FileCabinet> GetFileCabinets(IDataQuery query = null)
        {
            return _dataFacade.GetTableData<FileCabinet>(query, _tableName);
        }

        public bool AddFileCabinet(FileCabinet fileCabinet)
        {
            return _dataFacade.AddObject<FileCabinet>(fileCabinet, _tableName);
        }

        public void DeleteFileCabinet(string guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var fcList = _dataFacade.GetTableData<FileCabinet>(_dq, _tableName);
            if(fcList.Count > 0)
            {
                fcList[0].Deleted = true;
                _dataFacade.ModifyObject<FileCabinet>(fcList[0], _dq, _tableName);
            }
        }

        public FileCabinet UpdateFileCabinet(FileCabinet updatedFileCabinet)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", updatedFileCabinet.Guid.ToString());

            _dataFacade.ModifyObject<FileCabinet>(updatedFileCabinet, _dq, _tableName);

            return updatedFileCabinet;
        }

        public int GetCount(IDataQuery query)
        {
            return _dataFacade.GetTableRowCount(_tableName, query);
        }
    }
}