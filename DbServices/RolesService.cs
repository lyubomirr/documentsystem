﻿using Interfaces.Data;
using Interfaces.Services;
using Models;
using Ninject;
using System.Collections.Generic;

namespace DbServices
{
    public class RolesService : DbService, IRolesService
    {
        public RolesService(IDataFacade dataFacade, IDataQuery dq,
            [Named("RolesTableName")] string tableName)
            : base(dataFacade, dq, tableName) { }

        public List<Role> GetRoles(IDataQuery query = null)
        {
            return _dataFacade.GetTableData<Role>(query, _tableName);
        }
    }
}