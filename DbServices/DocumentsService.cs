﻿using Interfaces.Services;
using Interfaces.Data;
using Models;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using System;

namespace DbServices
{
    public class DocumentsService : DbService, IDocumentsService
    {
        private IFileCabinetsService _fileCabinetsService;
        private string[] _searchProperties = { "Name", "Description", "Subject", "Contact", "Company", "RegDocNumber" };     

        public DocumentsService(IDataFacade dataFacade,IDataQuery dq, 
            [Named("DocumentsTableName")] string tableName, IFileCabinetsService fileCabinetsService) 
            : base(dataFacade, dq, tableName)
        {
            _fileCabinetsService = fileCabinetsService;
        }

        public List<Document> GetDocuments(IDataQuery query = null)
        {
            if(query != null && !String.IsNullOrEmpty(query.GetJoinedTable()))
            {
                return _dataFacade.GetJoinedTableData<Document, User>(query, _tableName, "Owner");
            }
            else
            {
                return _dataFacade.GetTableData<Document>(query, _tableName);
            }
        }

        public Document GetDocumentById(string guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);
            var docs = GetDocuments(_dq);
            if(docs.Count == 0)
            {
                return null;
            }
            return docs[0];
        }

        public bool AddDocument(Document doc)
        {
            return _dataFacade.AddObject<Document>(doc, _tableName);
        }

        public Document UpdateDocument(Document updatedDoc)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", updatedDoc.Guid.ToString());

            _dataFacade.ModifyObject<Document>(updatedDoc, _dq, _tableName);

            return updatedDoc;
        }

        public void DeleteDocument(Document doc)
        {
            if(!doc.Archived)
            {
                doc.Deleted = true;
                UpdateDocument(doc);
            }
        }

        public void DeleteDocuments(IEnumerable<string> guids)
        {
            _dq.Init(_tableName)
                .AddQuery("Archived", 0)
                .AddQuery("Deleted", 0);

            var docs = GetDocuments(_dq);
            foreach (var doc in docs)
            {
                if (guids.Contains(doc.Guid.ToString()))
                {
                    doc.Deleted = true;
                    UpdateDocument(doc);
                }
            }
        }

        public void ArchivateDocument(Document doc)
        {
            if(!doc.Deleted)
            {
                doc.Archived = true;
                UpdateDocument(doc);
            }
        }

        public void ArchivateDocument(string guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);

            var docs = GetDocuments(_dq);
            if (docs.Count > 0)
            {
                docs[0].Archived = true;
                UpdateDocument(docs[0]);
            }
        }

        public void RestoreArchivated(IEnumerable<string> guids)
        {
            _dq.Init(_tableName)
                .AddQuery("Archived", 1);

            var docs = GetDocuments(_dq);
            foreach (var doc in docs)
            {
                if (guids.Contains(doc.Guid.ToString()))
                {
                    doc.Archived = false;
                    UpdateDocument(doc);
                }
            }
        }

        public void RestoreAllArchivated()
        {
            _dq.Init(_tableName)
                .AddQuery("Archived", 1);

            var docs = GetDocuments(_dq);
            var guids = docs.Select(doc => doc.Guid.ToString());
            RestoreArchivated(guids);
        }

        public void RestoreDeleted(IEnumerable<string> guids)
        {
            _dq.Init(_tableName)
                .AddQuery("Deleted", 1);

            var docs = GetDocuments(_dq);
            foreach(var doc in docs)
            {
                if(guids.Contains(doc.Guid.ToString()))
                {
                    doc.Deleted = false;
                    UpdateDocument(doc);

                    _dq.Init("FileCabinets")
                        .AddQuery("Guid", doc.FileCabinetId.ToString());

                    var fcs = _fileCabinetsService.GetFileCabinets(_dq);

                    if (fcs.Count > 0)
                    {
                        if (fcs[0].Deleted) //If the whole file cabinet of the file is deleted we restore it as well.
                        {
                            fcs[0].Deleted = false;
                            _fileCabinetsService.UpdateFileCabinet(fcs[0]);
                        }
                    }
                }
            }
        }

        public void RestoreAllDeleted()
        {
            _dq.Init(_tableName)
                .AddQuery("Deleted", 1);

            var docs = GetDocuments(_dq);
            var guids = docs.Select(doc => doc.Guid.ToString());
            RestoreDeleted(guids);
        }

        public int GetDocumentCount(IDataQuery dq)
        {
            return _dataFacade.GetTableRowCount(_tableName, dq);
        }
    }
}