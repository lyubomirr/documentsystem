﻿using Interfaces.Data;
using Interfaces.Services;
using Models;
using Ninject;
using System.Collections.Generic;


namespace DbServices
{
    public class UsersService : DbService, IUsersService
    {
        public UsersService(IDataFacade dataFacade, IDataQuery dq, 
            [Named("UsersTableName")] string tableName)
            : base(dataFacade, dq, tableName) { }

        public List<User> GetUsers(IDataQuery query = null)
        {
            return _dataFacade.GetTableData<User>(query,_tableName);
        }

        public User GetUserById(string guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var users = GetUsers(_dq);

            if(users.Count == 0)
            {
                return null;
            }

            return users[0];
        }
    }
}